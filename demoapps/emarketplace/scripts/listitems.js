var transform_to_list =
            { "tag": "li", "children": [

				{ "tag": "b", "html": "Item: ${itemname}<br>" },
				{ "tag": "b", "html": 'Time Remaining: <div id="countdown${_id.$oid}"></div><script> setInterval(change, 1000);' +
				'function change() { var elem = document.getElementById("countdown${_id.$oid}");' +
				' var currTime = new Date().getTime()/1000; var countdown = parseInt(${endingtime} - currTime);' +
				' if(countdown > 0){' +
				' var hms = toHMS(countdown);' + 
				' elem.innerHTML = hms;}else{endItem("${_id.$oid}", "${image}"); elem.innerHTML = "expired!";}}' +
				' </script><br>' },
				{ "tag": "span", "html": "Name: ${username}<br>" },
				{ "tag": "span", "html": "Email: ${email}<br>" },
				{ "tag": "span", "html": "Phone Number: ${phonenumber}<br>" },
				{ "tag": "span", "html": "Current Bid: \$${currentbid}<br>" },
				{ "tag": "span", "html": "Bidder: ${bidder.name}, ${bidder.email}, ${bidder.phonenumber}<br>" },
				{ "tag": "span", "html": "<br><img src=${image} height=\"100\" width=\"100\"></a><br>" },
				{ "tag": "span", "html": "${description}<br><br>" },
                { "tag": "span", "html": "<form action=\"biditem.html?item=${_id.$oid}\" method=\"post\" " +
                "onsubmit=\"setCurrentBid(${currentbid}, '${bidder.email}', '${bidder.name}')\"> <input type=\"submit\" name=\"submit\" value=\"Bid\"></form><p></p>" }
                ]
                
			};
		
	
			
			
			function toHMS(totalSec)
			{
				hours = parseInt( totalSec / 3600 ) % 24;
				minutes = parseInt( totalSec / 60 ) % 60;
				seconds = totalSec % 60;

				result = (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds);
				return result;
			}
			
			
			function endItem(objId, imgurl)
			{
				// TODO: send delete request for picture
				realUrl = imgurl.split("?")[0];
				$.ajax({
            		type: 'DELETE', 
            		beforeSend: function (request)
            		{
                		request.setRequestHeader("apikey", "515da909e4b011c53d41ded0");
            		},
            		url: realUrl,
            		dataType: 'json'
        		});
				
				// TODO: send delete request for item
				$.ajax({
            		type: 'DELETE', 
            		beforeSend: function (request)
            		{
                		request.setRequestHeader("apikey", "515da909e4b011c53d41ded0");
                		request.setRequestHeader("query", '{ "_id" : { "$oid" : "' + objId + '" } }');
            		},
            		url: "http://cloudhub.tk:8080/api/rest/1070807131204/eMarketplace/items",
            		dataType: 'json'
        		});

			}
			
			function httpGet(theUrl)
    
			{
    
				var xmlHttp = null;

    
				xmlHttp = new XMLHttpRequest();
    
				xmlHttp.open( "GET", theUrl, false );
    
				xmlHttp.send( null );
    
				return xmlHttp.responseText;  
			}
			
			var allFiles = httpGet('http://cloudhub.tk:8080/api/rest/1070807131204/eMarketplace/items?apikey=515da909e4b011c53d41ded0&sort={"endingtime":1}');
			var html3 = json2html.transform(allFiles, transform_to_list);
	
			document.write(html3 + "<br><br>");

    

			function setCurrentBid(bid, bidderemail, biddername) 
			{
				localStorage.setItem("currentbid", bid);
				localStorage.setItem("bidderemail", bidderemail);
				localStorage.setItem("biddername", biddername);
			}

