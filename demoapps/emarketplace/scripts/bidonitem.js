
    var QueryString = function () 
    {
  		// This function is anonymous, is executed immediately and 
  		// the return value is assigned to QueryString!
  		var query_string = {};
  		var query = window.location.search.substring(1);
  		var vars = query.split("&");
  
  
  		for (var i=0;i<vars.length;i++) 
  		{
    		var pair = vars[i].split("=");
    		// If first entry with this name
    		if (typeof query_string[pair[0]] === "undefined") 
    		{
      			query_string[pair[0]] = pair[1];
    			// If second entry with this name
    		} 
    		else if (typeof query_string[pair[0]] === "string") 
    		{
      			var arr = [ query_string[pair[0]], pair[1] ];
     	 		query_string[pair[0]] = arr;
    			// If third or later entry with this name
    		} 
    		else 
    		{
      			query_string[pair[0]].push(pair[1]);
    		}
  		} 
    	return query_string;
	} ();
    
    
    
    function httpPut(theUrl, newBid, bname, email, phonenumber )
    {
    	var id = QueryString.item;
    	var currentBid = localStorage.currentbid;
		var response = null;

    	if(parseInt(newBid, 10) <= parseInt(currentBid, 10))
    	{
    		alert("Bid must be higher than current bid of " + currentBid + "!");
    		return null;
    	}
    	if(newBid=="" || bname=="" || email=="" || phonenumber=="")
    	{
    		alert("You must fill out all fields!");
    		return null;
    	}
        
      	var query = new Object;
      	var oid = new Object;
      	oid["$oid"] = id;
      	query["_id"] = oid;
      	var update = new Object;
      	var bidder = new Object;
      	bidder["name"] = bname;
      	bidder["phonenumber"] = phonenumber;
      	bidder["email"] = email;
      	update["currentbid"] = newBid;
      	update["bidder"] = bidder;
	
		$.ajax({
            type:"PUT",
            async: false, 
            beforeSend: function (request)
            {
                request.setRequestHeader("apikey", "515da909e4b011c53d41ded0");
                request.setRequestHeader("query", JSON.stringify(query));
    			request.setRequestHeader("update", JSON.stringify(update));
            },
            url: theUrl,
            dataType: "json",
            success: function(msg) {
                response = msg;
            },
            complete: function() {
            	if(response==null)
        		{
        			alert("response is null!");
        		}
            }
    	});
    	

    	return response;
    }
    
    function sendEmail(addr, subj, mesg)
    {
    	var response = null;
    	$.ajax({
            type: 'POST',
            async: false, 
            url: "http://cloudhub.tk:8080/api/email/1070807131204/eMarketplace",
            dataType: 'json',
            data: {
                to: addr,
                from: 'noreply@emarketp.tk',
                subject: subj,
                message: mesg,
                apikey: "515da909e4b011c53d41ded0"
            },
            success: function (data) {
                response = data;
            }
        });
    	return response;
    }
    
    function updateBid()
    {
        var username = document.getElementById('username').value;
        var email = document.getElementById('email').value;
        var phonenumber = document.getElementById('phonenumber').value;
        var newbid = document.getElementById('newbid').value;
        
        var response = httpPut("http://cloudhub.tk:8080/api/rest/1070807131204/eMarketplace/items",
        newbid, username, email, phonenumber);
        
        if( response!=null)
        {
        	// send email to previous highest bidder
        	sendEmail(localStorage.bidderemail, 'You Have Been Outbid', localStorage.biddername + ',\nYou have been outbid with a new bid of $' + newbid + '! Place another bid before the time expires on the "' + response[0].itemname + '" item.\n\nRegards,\neMarketplace team');
        	// send email to current bidder
        	sendEmail(email, 'Successful Bid', 'Congratulations ' + username + '!\n You have successfully bid on an item "' + response[0].itemname + '" for $' + newbid + '. We will send you an email if you are outbid.\n\nRegards,\neMarketplace team' );
        	// send email to owner
        	sendEmail(response[0].email, 'Another Bid', 'Congratulations ' + response[0].username + '!\n\n Someone has placed a bid on your item "' + response[0].itemname + '" for $' + newbid + '.\n\nRegards,\neMarketplace team');
        	return true;
        }
        else
        {
        	return false;
        }
    }
