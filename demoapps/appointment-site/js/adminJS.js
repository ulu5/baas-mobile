// JavaScript Document
var ID = "1070807131204";
var appName = "app2";
var appapikey = "5171c53ae4b0237bf8c51009";
var tableName = "appointments";

//Admin-Create appointments
function createAppoint(appDate, appTime)
{
	hideCode();
	if(appDate == "" || appTime == "")
	{
		alert("Must enter a date and a time.");
		return;
	}
	//send the post request to cloudhub
	$.ajax({
    	type: 'POST',
        url: "http://cloudhub.tk:8080/api/rest/" + ID + "/" + appName + "/" + tableName,
        dataType: 'json',
		data: {
				apikey : appapikey,
				email: "",
				phoneNumber: "",
                status: "open",
				time: appTime,
				date: appDate,
            },
        success: function (data) {
			//Display returned JSON
			var jString = "<pre class=\"brush: js\">\r\n//The JSON that was returned\r\n";
			var jS = JSON.stringify(data, null, "\t");
			jS = JSON.stringify(data, null, 4);
			jString += jS;
			jString += "</pre>";	
			document.getElementById('jsonReturn').innerHTML = jString;
			$('#scheduleApps').show();	
			$('#jsonReturn').show();
			SyntaxHighlighter.highlight();			
        },
        error: function (xhr, type) {
			alert("There was a problem. Please try again");
        }
	});
}
//Get the scheduled appointments
function getAppointments(appDate)
{
	hideCode();
	if(appDate == "")
	{
		alert("Must enter a date.");
		return;
	}	
	//get appointments
	$.ajax({
    	type: 'GET',
        url: "http://cloudhub.tk:8080/api/rest/" + ID + "/" + appName + "/" + tableName,
        dataType: 'json',
		data: {
				apikey : appapikey,
                query : "{ date : \"" + appDate + "\"}"
            },
        success: function (data) {
			parseDates(data);
        },
        error: function (xhr, type) {
			alert("There was a problem. Please try again");
        }
	});
}
//parse and show the dates
function parseDates(data)
{	
	
	var htmlS = "";
	//show the appointmnets
	for(var i = 0; i < data.length; i++)
	{
		//get the current appointmnet
		var currentApp = data[i];
		if(currentApp.status == "open")
			htmlS += "<button type=\"button\" class=\"button success radius\" style=\"margin-top:0px; margin-right:10px;\" >" + currentApp.time + "</button>";
		else
			htmlS += "<button type=\"button\" class=\"button alert radius\" style=\"margin-top:0px;  margin-right:10px;\">" + currentApp.time + "</button>";
	}
	if(data.length == 0)
		var htmlS = "<b>There are no appointments for this date.</b>";
	$("#schApps").html(htmlS);	
	$("#appsByDay").show();
	//Display returned JSON and code snippets
	var jString = "<pre class=\"brush: js\">\r\n//The JSON that was returned\r\n";
	var jS = JSON.stringify(data, null, "\t");
	jS = JSON.stringify(data, null, 4);
	jString += jS;
	jString += "</pre>";	
	document.getElementById('jsonReturn').innerHTML = jString;
	$('#getAppointments').show();	
	$('#jsonReturn').show();
	SyntaxHighlighter.highlight();	
}

//hides all code snippets jsonReturn
function hideCode()
{
	$('#appsByDay').hide();
	$('#getAppointments').hide();
	$('#scheduleApps').hide();
	$('#jsonReturn').hide();

}


