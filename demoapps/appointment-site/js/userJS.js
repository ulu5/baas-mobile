// JavaScript Document
var ID = "1070807131204";
var appName = "app2";
var appapikey = "5171c53ae4b0237bf8c51009";
var tableName = "appointments";

//gets all the available appointments
function getAppoints(appDate)
{
	//get appointments
	$.ajax({
    	type: 'GET',
        url: "http://cloudhub.tk:8080/api/rest/" + ID + "/" + appName + "/" + tableName + "/",
        dataType: 'json',
		data: {
				apikey : appapikey,
                query : "{ date : \"" + appDate + "\", status : \"open\"}"
            },
        success: function (data) {
			showAppointments(data);
        },
        error: function (xhr, type) {
			alert("There was a problem. Please try again");
        }
	});				
}
//show all the dates available
function showAppointments(data)
{
	var htmlS = "";
	//show the appointmnets
	for(var i = 0; i < data.length; i++)
	{
		//get the current appointmnet
		var currentApp = data[i];
		htmlS += "<button type=\"button\" class=\"button success radius\" style=\"margin-top:0px; margin-right:10px;\" value=\"" + currentApp._id.$oid + "\" onClick=\"showInputForm(this.value, this.textContent )\">" + currentApp.time + "</button>";
	}
	if(data.length == 0)
		var htmlS = "<b>There are no appointments for this date.</b>";
	document.getElementById('lc').innerHTML = htmlS;
	
	//hide old code snippets
	hideCode();
	//Display returned JSON
	var jString = "<pre class=\"brush: js\">\r\n//The JSON that was returned\r\n";
	var jS = JSON.stringify(data, null, "\t");
	jS = JSON.stringify(data, null, 4);
	jString += jS;
	jString += "</pre>";	
	document.getElementById('jsonReturn').innerHTML = jString;
	$('#getApps').show();	
	$('#jsonReturn').show();
	SyntaxHighlighter.highlight();
	
	
}
//show form box
function showInputForm(rowID, time)
{
	document.getElementById('rID').value = rowID;
	document.getElementById('appTime').value = time;
	document.getElementById('appDate').value = document.getElementById('dp').value;
	$('#scheduleModal').foundation('reveal', 'open');
}
var email = "";
var phone = "";
var date = "";
var time = "";

//schedule the appointment
function scheduleAppoint(rowID, custEmail, custPhone, custDate, custTime)
{
	email = custEmail;
	phone = custPhone;
	date = custDate;
	time = custTime;
	
	$.ajax({
    	type: 'PUT',
		async: false,
        url: "http://cloudhub.tk:8080/api/rest/" + ID + "/" + appName + "/" + tableName + "/",
		contentType: "application/json",
        dataType: 'json',
		headers: {
				apikey : appapikey,
				query : "{\"_id\":{\"$oid\":\"" + rowID + "\"}}",
				update : "{ status : \"closed\", email : \"" + custEmail + "\", phoneNumber : \"" + custPhone + "\"}",
            },
        success: function (data) {
			$('#scheduleModal').foundation('reveal', 'close');
			//show code snippets
			//hide old code snippets
			hideCode();
			//Display returned JSON
			var jString = "<pre class=\"brush: js\">\r\n//The JSON that was returned for scheduling appointments\r\n";
			var jS = JSON.stringify(data, null, "\t");
			jS = JSON.stringify(data, null, 4);
			jString += jS;
			jString += "</pre>";	
			document.getElementById('jsonReturn').innerHTML = jString;
			$('#scheduleApp').show();	
			$('#jsonReturn').show();
			SyntaxHighlighter.highlight();
			//send email  if applicable
			if(email != "")
				sendEmail(email, date, time);	
			//send SMS message if applicable
			if(phone != "")
				sendSMS(phone, date, time);	
			document.getElementById('lc').innerHTML = "";
		},
        error: function (xhr, type) {
			alert("There was a problem. Please try again");
        }
	});
}

//Send email confirmation
function sendEmail(email, date, time)
{		
		$.ajax({
    	type: 'POST',
        url: "http://cloudhub.tk:8080/api/email/" + ID + "/" + appName + "/",
        dataType: 'json',
		data: {
				apikey : appapikey,
                clientid: ID,				
				appname: appName,
				to: email,
				subject : "Appointment",
				message : "You have an appointment on " + date + " at " + time + " ",				
            },
        success: function (data) {
			//Display returned JSON
			var jString = "<pre class=\"brush: js\">\r\n//The JSON that was returned from sendin email\r\n";
			var jS = JSON.stringify(data, null, "\t");
			jS = JSON.stringify(data, null, 4);
			jString += jS;
			jString += "</pre>";	
			document.getElementById('jsonEmail').innerHTML = jString;
			$('#SendEmail').show();	
			$('#jsonEmail').show();
			SyntaxHighlighter.highlight();			
        },
        error: function (xhr, type) {
			alert("There was a problem. Please try again");
        }
	});
}

//Send SMS confirmation
function sendSMS(phoneNum, date, time)
{
		$.ajax({
    	type: 'POST',
        url: "http://cloudhub.tk:8080/api/sms/" + ID + "/" + appName + "/",
        dataType: 'json',
		data: {
				apikey : appapikey,
                clientid: ID,				
				appname: appName,
				phonenumber: phoneNum,
				message : "You have an appointment on " + date + " at " + time + " ",					
            },
        success: function (data) {
			//Display returned JSON
			var jString = "<pre class=\"brush: js\">\r\n//The JSON that was returned from sendin SMS\r\n";
			var jS = JSON.stringify(data, null, "\t");
			jS = JSON.stringify(data, null, 4);
			jString += jS;
			jString += "</pre>";	
			document.getElementById('jsonSMS').innerHTML = jString;
			$('#SendSMS').show();	
			$('#jsonSMS').show();
			SyntaxHighlighter.highlight();
        },
        error: function (xhr, type) {
			alert("There was a problem. Please try again");
        }
	});
}

//hides all code snippets
function hideCode()
{
	$('#getApps').hide();
	$('#scheduleApp').hide();
	$('#SendEmail').hide();
	$('#SendSMS').hide();
	$('#jsonReturn').hide();
	$('#jsonEmail').hide();
	$('#jsonSMS').hide();
}

