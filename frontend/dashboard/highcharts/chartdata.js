// JavaScript Document
//http://cloudhub.tk:8080/api/app?apikey=1
var apicalls;
var apiStartDate;
var smscalls;
var smsStartDate;

function getApiCalls(appkey, clientID, appName)
{
	var appApiCalls;
	var appSMSCalls;
	var appUsers;
	$.ajax({
            type: 'GET',
            url: "http://" + JS.domain + "/api/app",
            dataType: 'json',
            data: {
                apikey: appkey,
				clientid: clientID,
				appname: appName,
            },
            success: function (data) {
                appApiCalls = data.apicount;
				appSMSCalls = data.smscount;
				appUsers = data.KPI;
            },
            complete: function () {
                apicalls = [];
				parseAPIDates(appApiCalls);		
				loadApiChart();				
				smscalls = [];
				parseSMSDates(appSMSCalls);
				parseKPI(appUsers);
				if(smsStartDate)
					loadSMSChart();			
            }
        });			
}

function parseAPIDates(appApiCalls)
{
	var currentDate;
	for(var key in appApiCalls)
	{
		if(key == "$") continue;
			
		var d = key.split("_");
		var TempDate = new Date(d[0],d[1], d[2]);
		if(!currentDate)
		{
			apiStartDate = TempDate;
			currentDate = TempDate;
		}
		else
		{
			while(true)
			{
				var nextDay = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate()+1);
				if(nextDay.getTime() != TempDate.getTime())
				{
					apicalls.push(0);
					currentDate = nextDay;
				}
				else
				{
					currentDate = TempDate;
					break;
				}
			}			
		}		
		apicalls.push(appApiCalls[key]);
	}	
}
function getApiData()
{
	return apicalls;
}
function getApiStartDate()
{
	return  Date.UTC(apiStartDate.getFullYear(), apiStartDate.getMonth(), apiStartDate.getDate());
}

function loadApiChart() {
    var chart;
		
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'dailyAPI',
                zoomType: 'x',
                spacingRight: 20
            },
            title: {
                text: 'Daily API Calls'
            },
            subtitle: {
                text: document.ontouchstart === undefined ?
                    'Click and drag in the plot area to zoom in' :
                    'Drag your finger over the plot to zoom in'
            },
            xAxis: {
                type: 'datetime',
                maxZoom: 14 * 24 * 3600000, // fourteen days
                title: {
                    text: null
                }
            },
            yAxis: {
                title: {
                    text: ''
                },
                showFirstLabel: false
            },
            tooltip: {
                shared: true
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1},
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, 'rgba(2,0,0,0)']
                        ]
                    },
                    lineWidth: 1,
                    marker: {
                        enabled: false,
                        states: {
                            hover: {
                                enabled: true,
                                radius: 5
                            }
                        }
                    },
                    shadow: false,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                }
            },
    
            series: [{
                type: 'area',
                name: 'API Calls',
                pointInterval: 24 * 3600 * 1000,
                pointStart: getApiStartDate(), //Date.UTC(2006, 0, 01),
                data: getApiData()
            }]
        });    
}
//SMS section
function parseSMSDates(appSMSCalls)
{
	var currentDate;
	for(var key in appSMSCalls)
	{
		if(key == "$") continue;
		var d = key.split("_");
		var TempDate = new Date(d[0],d[1], d[2]);
		if(!currentDate)
		{
			smsStartDate = TempDate;
			currentDate = TempDate;
		}
		else
		{
			while(true)
			{
				var nextDay = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate()+1);
				if(nextDay.getTime() != TempDate.getTime())
				{
					smscalls.push(0);
					currentDate = nextDay;
				}
				else
				{
					currentDate = TempDate;
					break;
				}
			}			
		}		
		smscalls.push(appSMSCalls[key]);
	}	
}
function getSMSData()
{
	return smscalls;
}
function getSMSStartDate()
{
	return  Date.UTC(smsStartDate.getFullYear(), smsStartDate.getMonth(), smsStartDate.getDate());
}

function loadSMSChart() {
    var chart;
		
        chart = new Highcharts.Chart({
            chart: {
                renderTo: 'smsCalls',
                zoomType: 'x',
                spacingRight: 20
            },
            title: {
                text: 'Daily SMS Calls'
            },
            subtitle: {
                text: document.ontouchstart === undefined ?
                    'Click and drag in the plot area to zoom in' :
                    'Drag your finger over the plot to zoom in'
            },
            xAxis: {
                type: 'datetime',
                maxZoom: 14 * 24 * 3600000, // fourteen days
                title: {
                    text: null
                }
            },
            yAxis: {
                title: {
                    text: ''
                },
                showFirstLabel: false
            },
            tooltip: {
                shared: true
            },
            legend: {
                enabled: false
            },
            plotOptions: {
                area: {
                    fillColor: {
                        linearGradient: { x1: 0, y1: 0, x2: 0, y2: 1},
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, 'rgba(2,0,0,0)']
                        ]
                    },
                    lineWidth: 1,
                    marker: {
                        enabled: false,
                        states: {
                            hover: {
                                enabled: true,
                                radius: 5
                            }
                        }
                    },
                    shadow: false,
                    states: {
                        hover: {
                            lineWidth: 1
                        }
                    },
                    threshold: null
                }
            },
    
            series: [{
                type: 'area',
                name: 'SMS Calls',
                pointInterval: 24 * 3600 * 1000,
                pointStart: getSMSStartDate(), //Date.UTC(2006, 0, 01),
                data: getSMSData()
            }]
        });    
}

function parseKPI(KPIData)
{
	var acq = 0;
	var eng = 0;
	var ret = 0;	
	var today = new Date();
	for(var key in KPIData)
	{
		var user = KPIData[key];
		var signupDate = new Date(user.signupdate*1000);
		var lastLoginDate = new Date(user.lastlogindate*1000); 
		if(today.getTime() - signupDate < 2592000000)
			acq ++;	
		else if(today.getTime() - lastLoginDate < 2592000000)
			eng ++;
		else
			ret ++;		
	}
	document.getElementById("Acq").innerHTML = acq;
	document.getElementById("Eng").innerHTML = eng;
	document.getElementById("Ret").innerHTML = ret;	
}