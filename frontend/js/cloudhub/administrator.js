// JavaScript Document
var ID = "1040113141210";
var appName = "IssuesLive";
var tableName = "issue"
var appapikey = "5171a68be4b00c9ef5bd1fc2";

//gets the open issues 
function getOpenIss()
{
	$("#pendingIssView").slideUp();
	
	$.ajax({
    	type: 'GET',
        url: "http://" + JS.domain + "/api/rest/" + ID + "/" + appName + "/" + tableName + "/",
        dataType: 'json',
		data: {
				apikey : appapikey,
                query : "{ status : \"open\"}"
            },
        success: function (data) {
			parseOpenIssues(data);
        },
        error: function (xhr, type) {
        }
	});
	
}
//parse and show all open issues
function parseOpenIssues(data)
{
	//holds the html string for the div
	var htmlS = "<h3>Open Issues</h3>";
	//show the open issues
	for(var i = 0; i < data.length; i++)
	{
		var customerIssue = data[i];
		htmlS += "<form>";
		//get customer id and issue
		htmlS += "<hr/><h5>Customer ID: " + customerIssue.clientid + "</h5>";
		htmlS += "<h4>Issue</h4><textarea id=\"issueText\" readonly>" + customerIssue.issue + "</textarea>";
		//Resonse Box
		htmlS += "<h4>Response</h4><textarea id=\"responseText\" placeholder=\"Write response.\"></textarea>";
		//hidden id
		htmlS += "<input id=\"rowID\" style=\"visibility:hidden\" value=\"" + customerIssue._id.$oid + "\"></input>";
		//resolved/pending buttons
		htmlS += "<button class=\"update\" type=\"button\" onClick=\"markAsPending(this.form.rowID.value);\">Mark as Pending</button>";
		htmlS += "<button class=\"update\" type=\"button\" style=\"margin-left:10px;\" onClick=\"markAsResolved(this.form.rowID.value, this.form.responseText.value);\">Mark as Resolved</button>";		
		htmlS += "</form>";
	}
		
	$("#openIssView").html(htmlS);	
	$("#openIssView").show("show");
}
//gets the open issues 
function getPendingIss()
{
	$("#openIssView").slideUp();	
	$.ajax({
    	type: 'GET',
        url: "http://" + JS.domain + "/api/rest/" + ID + "/" + appName + "/" + tableName + "/",
        dataType: 'json',
		data: {
				apikey : appapikey,
                query : "{ status : \"pending\"}"
            },
        success: function (data) {
			parsePendingIssues(data);
        },
        error: function (xhr, type) {
        }
	});
	
}
//parse and show all pending issues 
function parsePendingIssues(data)
{
	//holds the html string for the div
	var htmlS = "<h3>Pending Issues</h3>";
	//show the pending issues
	for(var i = 0; i < data.length; i++)
	{
		var customerIssue = data[i];
		htmlS += "<form>";
		//get customer id and issue
		htmlS += "<hr/><h5>Customer ID: " + customerIssue.clientid + "</h5>";
		htmlS += "<h4>Issue</h4><textarea id=\"issueText\" readonly>" + customerIssue.issue + "</textarea>";
		//Resonse Box
		htmlS += "<h4>Response</h4><textarea id=\"responseText\" placeholder=\"Write response.\"></textarea>";
		//hidden id
		htmlS += "<input id=\"rowID\" style=\"visibility:hidden\" value=\"" + customerIssue._id.$oid + "\"></input>";
		//resolved buttons
		htmlS += "<button class=\"update\" type=\"button\" style=\"margin-left:10px;\" onClick=\"markAsResolved(this.form.rowID.value, this.form.responseText.value);\">Mark as Resolved</button>";		
		htmlS += "</form>";
	}
	$("#pendingIssView").html(htmlS);	
	$("#pendingIssView").show("show");
}
//mark the issue as pending
function markAsPending(rowID)
{	
	$.ajax({
    	type: 'PUT',
        url: "http://" + JS.domain + "/api/rest/" + ID + "/" + appName + "/" + tableName + "/",
		contentType: "application/json",
        dataType: 'json',
		headers: {
				apikey : appapikey,
				query : "{\"_id\":{\"$oid\":\"" + rowID + "\"}}",
				update : "{ status : \"pending\"}",
            },
        success: function (data) {
			alert("Issue marked pending successfully!");
			window.location = "administrator.html";
        },
        error: function (xhr, type) {
			alert("There was a problem. Please try again");
        }
	});
}
//mark as resolved
function markAsResolved(rowID, response)
{
		$.ajax({
    	type: 'PUT',
        url: "http://" + JS.domain + "/api/rest/" + ID + "/" + appName + "/" + tableName + "/",
		contentType: "application/json",
        dataType: 'json',
		headers: {
				apikey : appapikey,
				query : "{\"_id\":{\"$oid\":\"" + rowID + "\"}}",
				update : "{ status : \"resolved\", response : \"" + response + "\"}",
            },
        success: function (data) {
			alert("Issue marked resolved successfully!");
			window.location = "administrator.html";
        },
        error: function (xhr, type) {
			alert("There was a problem. Please try again");
        }
	});
}