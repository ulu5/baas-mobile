// JavaScript Document
//For Account Page ticketing System
var ID = "1040113141210";
var appName = "IssuesLive";
var tableName = "issue"
var appapikey = "5171a68be4b00c9ef5bd1fc2";


//Sumbit new customer issue
function submitIssue()
{
	var issue = document.getElementById("issueText").value;
	if(!issue)
	{
		$('#issueText').attr('placeholder', 'You must write an issue!');
		return;
	}
	var clientID = JS.dashboard.getUserData().clientid;	
	var status = "open";	
	//send issue
	$.ajax({
    	type: 'POST',
        url: "http://" + JS.domain + "/api/rest/" + ID + "/" + appName + "/" + tableName + "/",
        dataType: 'json',
		data: {
				apikey : appapikey,
                clientid: clientID,
				issue: issue,
				status: status,
                response: "",
            },
        success: function (data) {
			alert("Issue submitted successfully!");
			window.location = "account.html";
        },
        error: function (xhr, type) {
			alert("There was a problem. Please try again");
        }
	});	
}
//Get and display all open Issues 
function getOpen()
{
	//Close all other Views
	$("#pendingView").slideUp();
	$("#resolvedView").slideUp();
	//holds the html string for the div
	var htmlS = "<h3>Open Issues</h3>";
	//Get the open issues and display them
	for(var i = 0; i < openIssues.length; i++)
	{
		htmlS += "<hr/><h4>Issue</h4><textarea readonly>" + openIssues[i].issue + "</textarea>";
	}
	$("#openView").html(htmlS);	
	$("#openView").show("show");

}
//Get and display all pending Issues 
function getPending()
{
	//Close all other Views
	$("#openView").slideUp();
	$("#resolvedView").slideUp();
	//holds the html string for the div
	var htmlS = "<h3>Pending Issues</h3>";
	//Get the pending issues and display them
	for(var i = 0; i < pendingIssues.length; i++)
	{
		htmlS += "<hr/><h4>Issue</h4><textarea readonly>" + pendingIssues[i].issue + "</textarea>";	
	}
	$("#pendingView").html(htmlS);	
	$("#pendingView").show("show");
}
//Get and display all resolved Issues 
function getResolved()
{
	//Close all other Views
	$("#pendingView").slideUp();
	$("#openView").slideUp();
	//holds the html string for the div
	var htmlS = "<h3>Resolved Issues</h3>";
	//Get the resolved issues and responses display them
	
	for(var i = 0; i < resolvedIssues.length; i++)
	{
		htmlS += "<hr/><h4>Issue</h4><textarea readonly>" + resolvedIssues[i].issue + "</textarea>";
		htmlS += "<h4>Response</h4><textarea readonly>" + resolvedIssues[i].response + "</textarea>";	
	}
	$("#resolvedView").html(htmlS);	
	$("#resolvedView").show("show");
}
//get all issues when the page loads
var openIssues;
var pendingIssues;
var resolvedIssues;


function getIssues()
{
	var clientID = JS.dashboard.getUserData().clientid;		
	//get issue
	$.ajax({
    	type: 'GET',
        url: "http://" + JS.domain + "/api/rest/" + ID + "/" + appName + "/" + tableName + "/",
        dataType: 'json',
		data: {
				apikey : appapikey,
                query : "{ clientid : \"" + clientID + "\"}"
            },
        success: function (data) {
			parseIssues(data);
        },
        error: function (xhr, type) {
        }
	});
}

function parseIssues(data)
{
    openIssues = [];
    pendingIssues = [];
    resolvedIssues = [];
	for(var i = 0; i < data.length; i++)
	{
		var issue = data[i];
		var status = issue.status;
		if(status == "open")
			openIssues.push(issue);
		else if(status == "pending")
			pendingIssues.push(issue);
		else if(status == "resolved")
			resolvedIssues.push(issue);	
		
	}
	document.getElementById('RLText').innerHTML = openIssues.length + " Open"
	document.getElementById('YLText').innerHTML = pendingIssues.length + " Pending"
	document.getElementById('GLText').innerHTML = resolvedIssues.length + " Resolved"
}

//update account info
function updateInfo(firstName, lastName, email, password)
{

	//get headers of changed	
	var clientID = JS.dashboard.getUserData().clientid;
	var myHeaders = {};		
		
	if(firstName != "")
	{
		myHeaders["firstname"] = firstName;
		var userObject = JSON.parse(localStorage.user);
		userObject['firstname'] = firstName;
		localStorage.setItem("user", JSON.stringify(userObject));
	}
	if(lastName != "")
	{
		myHeaders["lastname"] = lastName;
		var userObject = JSON.parse(localStorage.user);
		userObject['lastname'] = lastName;
		localStorage.setItem("user", JSON.stringify(userObject));
		
	}
	if(email != "")
	{
		myHeaders["email"] = email;
		var userObject = JSON.parse(localStorage.user);
		userObject['email'] = email;
		localStorage.setItem("user", JSON.stringify(userObject));
	}
	if(password != "")
	{
		myHeaders["password"] = email;
	}
	if (jQuery.isEmptyObject(myHeaders)) {
		alert("Nothing to update");
		return;
	}
	var myajax = {
		type: 'PUT',
		url: "http://" + JS.domain + "/api/user/" + clientID,
		dataType: 'json',
		data: {
                clientid: clientID,
        },
        success: function (data) {
			$("#imgLoading").hide();
			alert("Account info updated successfully");			
			window.location = "account.html";
        },
		error: function (xhr, type) {
			alert("Please try again later.");
			window.location = "account.html";
		}
	}
	//need to add headers somehow
	myajax.headers = myHeaders;
	//send request
	$.ajax(myajax);
}

function resetAPIKey() {
	var appIndex = $('#applist').get(0).selectedIndex;
    var apiKey = JSON.parse(localStorage.user).apps[appIndex].apikey;
	var clientId = JS.user.getClientId();
    var appName = $('#applist')[0].value;
	$("#imgLoading").show();
    $.ajax({    
        url: "http://" + JS.domain + "/app",
        contentType: "application/json",
        type: "PUT",
        dataType: "json",
        beforeSend: function (request) {
            request.setRequestHeader("clientid", clientId);
            request.setRequestHeader("appname", appName);
            request.setRequestHeader("apikey", apiKey);
        }
    }).done(function (data) {
		$("#imgLoading").hide();		
        alert("Update successful!");
    });			           
}
