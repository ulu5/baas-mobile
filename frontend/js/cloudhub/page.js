JS.page = (function () {
    
    function populateLastLogin() {
        $("#lastlogindate").text(JS.user.getLastLoginDate());
    }
    
    function populateWelcomeGreeting() {
        $("#dashfname").text(JS.dashboard.getFirstName());
    }
    
    function populateAppList() {
        var appListArray = JS.dashboard.getAppsList();
        for (var i = 0; i < appListArray.length; i++)
            $("#applist").append('<option id="' + appListArray[i].appname + '">' + appListArray[i].appname + '</option>');
		if(appListArray.length == 0 )
			$("#applist").append('<option>Select App</option>');
            
        $('#applist').append('<option id="addappoption">+ Add App</option>');
    }
    
    function addAppToAppList(data) {
        
        var key = JSON.parse(data).apikey;
        var appname = (JSON.parse(data).appname).substring(13);
        var tmp = JS.dashboard.getUserData();
        var newapp = { apikey: key, appname: appname, tables: [] };
        tmp.apps[tmp.apps.length] = newapp;
        localStorage.setItem('user', JSON.stringify(tmp));
        $('#successappname')[0].innerHTML = appname;
        $('#newapikey')[0].innerHTML = key;
        $('#addAppSuccessModal').reveal();
        $('#applist').html("");
        var appListArray = JS.dashboard.getAppsList();
        if(appListArray.length == 0) $('#applist').append('<option></option>');
        for (var i = 0; i < appListArray.length; i++)
            $("#applist").append('<option id="' + appListArray[i].appname + '">' + appListArray[i].appname + '</option>');

        // Add the 'Create App' option to the app dropdownlist
        $('#applist').append('<option id="addappoption">+ Add App</option>');
    }
    
    function populateStripeAccount() {
        
        var stripeListArray = JS.dashboard.getStripeAccounts();
        if(stripeListArray.length == 0) $('#stripeconnectlist').append('<option></option>');
        for (var i = 0; i < stripeListArray.length; i++)
            $("#stripeconnectlist").append('<option id="' + stripeListArray[i].stripe_user_id + '">' + stripeListArray[i].stripe_user_id + '</option>');
        
        $('#stripeconnectlist').append('<option id="addstripeaccount">+ Connect New Account</option>');
    }
    
    function redirectFromStripe() {
        if(JS.page.getURLParam("code") != "null") return true;
        return false;
    }
    
    function redirectToStripe() {
        window.location = "https://connect.stripe.com/oauth/authorize?response_type=code&scope=read_write&stripe_landing=login&client_id=" + JS.stripeAppKey;
    }
    
    function addNewStripeAccount() {
        var code = JS.page.getURLParam("code");
        JS.dashboard.addNewStripeAccount(code);
    }
    
    function getURLParam(name) {
        return decodeURI((RegExp(name + '=' + '(.+?)(&|$)').exec(location.search)||[,null])[1]);
    }
    
    function initDashboardPage() {
        populateLastLogin();
        populateWelcomeGreeting();
        populateAppList();
    }
    
    function displayLoginSignUpError(errormsg) {
        $('#error').detach();
        $('#parentError').append($('<div id="error">Error: ' + errormsg + '</div>'));
    }
    
    return {
        addAppToAppList: addAppToAppList,
        populateStripeAccount: populateStripeAccount,
        redirectFromStripe: redirectFromStripe,
        redirectToStripe: redirectToStripe,
        addNewStripeAccount: addNewStripeAccount,
        getURLParam: getURLParam,
        initDashboardPage: initDashboardPage,
        displayLoginSignUpError: displayLoginSignUpError
    };
    
})();
