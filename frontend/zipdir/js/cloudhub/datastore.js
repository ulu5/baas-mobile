JS.datastore = (function(){

	function getAppData(appName, docName, recordStart, recordEnd) {

		$.get({
			url: "http://" + JS.domain + "/api/" + JS.user.getClientId() + "/" + appName + "/" + docName, 
			{ start: recordStart, end: recordEnd }
		}).done(function (data) {
			return data;
		});
	}

    function createNewApp() {
        
        var postdata = "name=" + $('#newappname').val() + "&clientid=" + JS.user.getClientId();
        $.ajax({
			url: "http://" + JS.domain + "/api/app" , 
            type: "post",
            data: postdata
		}).done(function (data) {
			return data;
		});
    }
    
    return {
		getAppData: getAppData,
        createNewApp: createNewApp
	};

})();
