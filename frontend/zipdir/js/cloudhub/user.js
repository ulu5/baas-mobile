JS.user = (function () {

    function loginUser(uname, pass) {

        if(uname.length < 1 || pass.length < 1) {
            JS.page.displayLoginSignUpError("Please enter your E-Mail address and Password.");
            return;
        }

        if(!validateEmail(uname)) {
            JS.page.displayLoginSignUpError("Please enter a valid E-Mail address.");
            return;
        }
        
        $.ajax({
            type: 'GET',
            url: "http://" + JS.domain + "/api/user",
            dataType: 'json',
            data: {
                email: uname,
                password: toMD5(pass)
            },
            success: function (data) {
                
                if (data.success == 1) {
                    localStorage.setItem('user', JSON.stringify(data.user));
                    window.location = "dashboard/analytics.html";
                }
                else {
                    JS.page.displayLoginSignUpError(data.errmsg);
                }
            },
            error: function (xhr, type) {
                JS.page.displayLoginSignUpError("Please try again later.");
            }
        });
    }

    function signupUser(fname, lname, email, plan, pass1, pass2, ccname, ccadd1, ccadd2, cczip, cccity, ccstate, cccountry, ccno, cccvv, ccexpm, ccexpy) {

        if(fname.length < 1 || lname.length < 1 || email.length < 1 || pass1.length < 1 || pass2.length < 1) {
            JS.page.displayLoginSignUpError("Please enter all the information.");
            return;
        }

        if(!validateEmail(email)) {
            JS.page.displayLoginSignUpError("Please enter a valid E-Mail address.");
            return;
        }
        
        if (pass1 != pass2) {
            JS.page.displayLoginSignUpError("Passwords don't match.");
            return;
        }

        var stripe_token = null;

        if (plan != "Sandbox") {
            
            if(ccname.length < 1 || ccadd1.length < 1 || cccity.length < 1 || ccstate.length < 1 || cczip.length < 1 || cccountry.length < 1 || ccno.length < 1 || cccvv.length < 1 || ccexpm < 1 || ccexpy < 1) {
                JS.page.displayLoginSignUpError("Please enter all the information.");
                return;
            }
            
            // Charge user
            Stripe.setPublishableKey('pk_test_aBVFPigKPoybKrpLRC4IA5XB');
            Stripe.createToken({
                name: ccname,
                address_line1: ccadd1,
                address_line2: ccadd2,
                address_city: cccity,
                address_state: ccstate,
                address_zip: cczip,
                address_country: cccountry,
                number: ccno,
                cvc: cccvv,
                exp_month: ccexpm,
                exp_year: ccexpy
            }, function (status, response) {

                if (response.error) {
                    JS.page.displayLoginSignUpError(response.error.message);
                    return;
                }
                else {
                    stripe_token = response['id'];
                    signUpRequest(fname, lname, plan, email, pass1, stripe_token);
                }
            });
        }
        
        else {
            signUpRequest(fname, lname, plan, email, pass1, stripe_token);
        }
        
    }

    function signUpRequest(fname, lname, plan, email, pass1, stripe_token) {
        
        $.ajax({
            type: 'POST',
            url: "http://" + JS.domain + "/api/user",
            dataType: 'json',
            data: {
                firstname: fname,
                lastname: lname,
                plan: plan,
                email: email,
                password: toMD5(pass1),
                stripe_token: stripe_token
            },
            success: function (data) {

                if (data.success == 1) {

                    var user = {
                        firstname: fname,
                        lastname: lname,
                        email: email,
                        plan: plan,
                        signupdate: (new Date().getTime() / 1000),
                        lastlogindate: (new Date().getTime() / 1000)
                    };

                    localStorage.setItem('user', JSON.stringify(user));

                    window.location = "dashboard/analytics.html";
                }
                else {
                    JS.page.displayLoginSignUpError(data.errmsg);
                }
            },
            error: function (xhr, type) {
                JS.page.displayLoginSignUpError("Please try again later.");
            }
        });
    }

    function checkLoggedIn() {

        return ("user" in localStorage) ? true : false;
    }

    function logoutUser() {

        localStorage.removeItem('user');
        window.location = "../index.html";
    }
    
    function validateEmail(email) {
        
        var regex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return regex.test(email);
    }

    function getClientId() {

        return JSON.parse(localStorage.getItem('user')).clientid;
    }

    function getLastLoginDate() {

        return new Date(JSON.parse(localStorage.getItem('user')).lastlogindate * 1000);
    }

    return {

        loginUser: loginUser,
        signupUser: signupUser,
        checkLoggedIn: checkLoggedIn,
        logoutUser: logoutUser,
        validateEmail: validateEmail,
        getClientId: getClientId,
        getLastLoginDate: getLastLoginDate
    };

})();
