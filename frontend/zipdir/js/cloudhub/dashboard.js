JS.dashboard = (function() {

	function getUserData() {

		return JSON.parse(localStorage.getItem('user'));
	}

	function getFirstName() {

		return getUserData().firstname;
	}

	function getLastName() {

		return getUserData().lastname;
	}

	function getEmailAddress() {

		return getUserData().email;
	}

	function getAppsList() {

		return getUserData().apps;
	}

    function createNewApp(appname) {
        
        var postdata = "name=" + appname + "&clientid=" + JS.user.getClientId();
        $.ajax({
                url: "http://" + JS.domain + "/api/app",
                type: "post",
                data: postdata
            }).done(function (data) {
                JS.page.addAppToAppList(data);
        });
    }

    function getStripeAccounts() {
        
        return getUserData().stripeconnect;
    }

    function addNewStripeAccount(code) {
        
        var clientId = JS.user.getClientId();
        $.ajax({
                url: "http://" + JS.domain + "/payment/linkaccount",
                type: "get",
                data: { clientid: clientId, stripecode: code }
            }).done(function (data) {
                if(data.error == undefined) {
                    var usr = localStorage.getItem('user');
                    usr.stripeconnect[usr.stripeconnect.length+1] = data;
                    localStorage.setItem('user', usr);
                }
        });
    }

	return {

		getUserData: getUserData,
		getFirstName: getFirstName,
		getLastName: getLastName,
		getEmailAddress: getEmailAddress,
		getAppsList: getAppsList,
        createNewApp: createNewApp,
        getStripeAccounts: getStripeAccounts,
        addNewStripeAccount: addNewStripeAccount
	};

})();
