// JavaScript Document

var currentPlatform;//ios, android, or JS
var currentSnippet;//add, get, send


function showAPlatform(platform)
{
	if(currentPlatform == platform)
	{
		return;
	}
	if(currentPlatform == null)
	{
		$(platform).addClass('fadeInRightBig');
		$(platform).show();
		currentPlatform = platform;
		currentSnippet = platform + "AddObj";
		$(platform + "AddObj").show();
		return;
	}				
	//remove old platform
	$(currentPlatform).removeClass('fadeInRightBig');
	$(currentPlatform).addClass('fadeOutDownBig');
	$(currentPlatform).hide(500);
	//show new platform
	setTimeout(function(){
		$(platform + "AddObj").removeClass('fadeOutDownBig');
  		$(platform).removeClass('fadeOutDownBig');
		$(platform).addClass('fadeInRightBig');
		$(platform).show();
		$(currentSnippet).hide();
		currentSnippet = platform + "AddObj";
		$(platform + "AddObj").show();
		currentPlatform = platform;			
	}, 400);
}

function showSnippet(snippet)
{
	if(currentSnippet == currentPlatform + snippet)
	{
		return;
	}				
	//remove old snippet	
	$(currentSnippet).removeClass('fadeInRightBig');
	$(currentSnippet).addClass('fadeOutDownBig');
	$(currentSnippet).hide(500);
	//show new snippet
	setTimeout(function(){
  		$(currentPlatform + snippet).removeClass('fadeOutDownBig');
		$(currentPlatform + snippet).addClass('fadeInRightBig');
		$(currentPlatform + snippet).show();
		currentSnippet = currentPlatform + snippet;
		$(currentSnippet).show();			
	}, 400);


}
	