package com.baasmobile.plugins;

import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.baasmobile.util.Globals;
import com.baasmobile.util.Utils;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import com.sun.jersey.multipart.FormDataMultiPart;

@Path("/email/{clientid}/{appname}")
public class MailGun 
{	
	  public static final String FROM = "noreply-alt@cloudhub.tk";
	  public static final String API_KEY = "key-3gf2iqk6jeqy3xo0j7hzjf0nyphaoo99";
	  public static final String MSG_RESOURCE = "https://api.mailgun.net/v2/cloudhub.mailgun.org/messages";
	  
	  private static final String WELCOME = 
	  		"<br /><br /><h3>Welcome to CloudHub</h3><br />" +
	  		"Thank you for joining CloudHub. CloudHub takes care of all of your backend database<br />" +
	  		"needs and allows you as a developer to focus on the frontend. With simple REST calls,<br />" +
	  		"you will be able to interact with your backend storage through virtually any programming<br />" +
	  		"language on any platform.<br /><br />" +
	  		"Here are some links to help get started with cloudhub.<br /><br />" +
	  		"<a href=http://cloudhub.tk/docs.html>Documentation</a><br />" +
	  		"<a href=http://cloudhub.tk/gettingstarted.html>Code Snippets</a><br />" +
	  		"<a href=http://cloudhub.tk/pricing.html>Pricing</a><br />" +
	  		"<a href=http://cloudhub.tk/about.html>About Us</a><br /><br />" +
	  		"We hope you enjoy our services.<br /><br /><br />" +
	  		"Sincerely,<br /><br />" +
	  		"The CloudHub team<br /><br /><br />" +
	  		"<a href=http://cloudhub.tk>Click Here</a> to go to our site<br /><br />" +
	  		"<a href=http://cloudhub.tk><img src=http://cloudhub.tk/images/logo/slogo.png></a>" +
	  		"</html>";
	  
	  
	  /**
	   * Adds support if user wants to specify their own from field.
	   * 
	   * @param to
	   * @param from
	   * @param subject
	   * @param message
	   * @param clientId
	   * @param appName
	   * @param apiKey
	   * @return
	   */
	  @POST
	  @Produces(MediaType.APPLICATION_JSON)
	  public Response sendEmail(@FormParam("to") String to, 
			  @FormParam("from") String fromEmail, 
			  @FormParam("subject") String subject, 
			  @FormParam("message") String message,
			  @PathParam("clientid") String clientId,
			  @PathParam("appname") String appName,
			  @FormParam("apikey") String apiKey)
	  {
	  	try
	  	{
			if(!Utils.validateAPIKey(clientId, appName, apiKey))
				return Utils.unsuccessfulAllResponse("Invalid api key/client id/ appname combination!");
			
			if(fromEmail==null || fromEmail.isEmpty())
				fromEmail = FROM;
			  
			Client client = Client.create();
		    client.addFilter(new HTTPBasicAuthFilter("api", API_KEY));
		    WebResource webResource = client.resource(MSG_RESOURCE);
		    MultivaluedMapImpl formData = new MultivaluedMapImpl();
		    formData.add("from", fromEmail);
		    formData.add("to", to);
		    formData.add("subject", subject);
		    formData.add("text", message);
		    ClientResponse cr = webResource.type(MediaType.APPLICATION_FORM_URLENCODED).post(ClientResponse.class, formData);
		    return Utils.successfulAllResponse(cr.toString());
	  	}
	  	catch (Exception e)
	  	{
	  		e.printStackTrace();
			if(Globals.production)
				return Utils.unsuccessfulAllResponse("Please try again later!");
			else
				return Utils.unsuccessfulAllResponse(e.toString()); 
	  	}
	  }
	  
	  public static String sendWelcomeEmail(String to, String name)
	  {
		  Client client = Client.create();
	      client.addFilter(new HTTPBasicAuthFilter("api", API_KEY));
	      WebResource webResource = client.resource(MSG_RESOURCE);
	      FormDataMultiPart formData = new FormDataMultiPart();
	      formData.field("from", FROM);
	      formData.field("to", to);
	      formData.field("subject", "Welcome To CloudHub");
	      formData.field("html", "<html><b>" + name + ",</b>" + WELCOME);
	      ClientResponse cr = webResource.type(MediaType.MULTIPART_FORM_DATA_TYPE).post(ClientResponse.class, formData);
	      return cr.toString();
	  }
}
