package com.baasmobile.plugins;
import java.util.HashMap;
import java.util.Map;


import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.baasmobile.util.CountUtil;
import com.baasmobile.util.Globals;
import com.baasmobile.util.Utils;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.stripe.model.Charge;
import com.twilio.sdk.TwilioRestClient;
import com.twilio.sdk.resource.factory.SmsFactory;
import com.twilio.sdk.resource.instance.Account;


@Path("/sms/{clientid}/{appname}")
public class Twilio 
{
	public static final String ACCOUNT_SID = "AC8955b2776edb14baa3759004d7405572";
	public static final String AUTH_TOKEN = "2fd494c9c98a6ced725d42a2fb44e375";
	public static final String SRC_PHONENUMBER = "(646) 606-2482";
	  
	public static final HashMap<String, Integer> PLAN_SMS_CHARGES;
    static
    {
    	PLAN_SMS_CHARGES = new HashMap<String, Integer>();
    	PLAN_SMS_CHARGES.put("Sandbox", 5);
    	PLAN_SMS_CHARGES.put("Bootstrap", 3);
    	PLAN_SMS_CHARGES.put("Startup", 2);
    	PLAN_SMS_CHARGES.put("Enterprise", 1);
    }
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public Response sendSMS(@FormParam("phonenumber") String phonenumber, 
			  @FormParam("message") String message,
			  @FormParam("apikey") String apiKey,
			  @PathParam("clientid") String clientId,
			  @PathParam("appname") String appName)
	{
		try 
		{
			// validate api key
			if(!Utils.validateAPIKey(clientId, appName, apiKey))
			{
				return Utils.unsuccessfulAllResponse("Invalid api key/client id");
			}
				
			MongoClient mc = Utils.getAuthenticatedClient();
			
			// check valid credit card  
			DB db = mc.getDB(Globals.SYSTEM_DB_NAME);
			DBCollection users = db.getCollection(Globals.USERS_COLL_NAME);
			BasicDBObject query = new BasicDBObject("clientid", clientId);
			
			DBObject user = users.findOne(query);
			
			// TODO: add name on card?
			if(!user.containsField("creditcard") || !user.containsField("cvv2") 
					|| !user.containsField("expmonth") 
					|| !user.containsField("expyear") )
			{
				return Utils.unsuccessfulAllResponse("Credit card information incomplete");
			}
			else if(!Utils.validateCreditCard(user))
			{
				return Utils.unsuccessfulAllResponse("Invalid credit card on file");
			}
			  
			  
			// send the message
			TwilioRestClient twclient = new TwilioRestClient(ACCOUNT_SID, AUTH_TOKEN);
			  
			Account account = twclient.getAccount();
			SmsFactory sms = account.getSmsFactory();
			HashMap<String, String> smsParam = new HashMap<String, String> ();
			smsParam.put("To", phonenumber);
			smsParam.put("From", SRC_PHONENUMBER);
			smsParam.put("Body", message);

			sms.create(smsParam);
			
			// get charge per plan
			Integer amount = PLAN_SMS_CHARGES.get(user.get("plan"));
			
			// create card object
			Map<String, Object> cardDetails = new HashMap<String, Object>();
			cardDetails.put("number", user.get("creditcard"));
			cardDetails.put("exp_month", user.get("expmonth"));
			cardDetails.put("exp_year", user.get("expyear"));
			
			// TODO: charge according to plan
			Map<String, Object> chargeParams = new HashMap<String, Object>();
			
			chargeParams.put("amount", amount);
			chargeParams.put("currency", "usd");
			chargeParams.put("card", cardDetails);
			chargeParams.put("description", "sending a text from " + appName);
			
			Charge newCharge = Charge.create(chargeParams);
			// TODO: if(newCharge.getPaid())
			
			
			// increment daily sms count
			CountUtil.incrementTodaysSmsCount(apiKey, mc);
			
			mc.close();
			
			return Utils.successfulAllResponse("Sms sent successfully to " + phonenumber + "!");
		  }
		  catch(Exception e) 
		  {
	  		e.printStackTrace();
			if(Globals.production)
				return Utils.unsuccessfulAllResponse("Please try again later!");
			else
				return Utils.unsuccessfulAllResponse(e.toString()); 
		  }
		  
		  
	  }
}
