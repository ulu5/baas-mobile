package com.baasmobile.plugins;

import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.baasmobile.util.Globals;
import com.baasmobile.util.Utils;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.multipart.FormDataMultiPart;

@Path("/payment/linkaccount")
public class StripeConnect 
{
	/*
	 * Stripe Connect works as follows:
	 * 1. User clicks on "Connect with Stripe" on Account page
	 * 2. Stripe redirects to front page with "code"
	 * 3. If frontend receives "code", this REST Call with obtain token
	 * 4. Another REST function will act as a webhook for deauthorized action
	 * 5. Helper REST calls will be used for charges and refunds
	 * NOTE: Transaction fees for account can be assessed based on Plan 
	 * */
	
	private static String STRIPE_API_KEY = "sk_test_Wn1dgQX3dXdiOUxKwh7d21Fe";
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getDetailsFromCode(@QueryParam("clientid") String clientid, @QueryParam("code") String code) 
	{
		try
		{
		Client client = Client.create();
		WebResource webResource = client.resource("https://connect.stripe.com/oauth/token");
		FormDataMultiPart formData = new FormDataMultiPart();
		formData.field("client_secret", STRIPE_API_KEY);
		formData.field("code", code);
		formData.field("grant_type", code);
		ClientResponse cr = webResource.type(MediaType.MULTIPART_FORM_DATA_TYPE).post(ClientResponse.class, formData);
		Utils.storeStripeConnect(clientid, (DBObject) JSON.parse(cr.toString()));
	    return cr.toString();
		}
		catch(Exception e)
		{
			if (Globals.production)
				return "Error: Please try again later";
			return e.toString();
		}
	}
}
