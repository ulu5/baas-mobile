package com.baasmobile.beans;

import java.io.IOException;
import java.io.OutputStream;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.StreamingOutput;

import com.mongodb.gridfs.GridFSDBFile;

public class BaaSMobileStream implements StreamingOutput
{

	private GridFSDBFile myFile;
	
	public BaaSMobileStream(GridFSDBFile gridFile)
	{
		myFile = gridFile;
	}
	
	@Override
	public void write(OutputStream os) throws IOException,
			WebApplicationException
	{
		myFile.writeTo(os);
	}
	

}
