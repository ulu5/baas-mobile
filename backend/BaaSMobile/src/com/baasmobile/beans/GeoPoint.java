package com.baasmobile.beans;

/*
 * GeoPoint - Geolocation library for Cloud Hub
 * 
 * This uses the ‘haversine’ formula to calculate the great-circle distance
 * between two points – that is, the shortest distance over the earth’s surface
 * giving an ‘as-the-crow-flies’ distance between the points.
 * 
 * Formula: a = sin²(Δφ/2) + cos(φ1).cos(φ2).sin²(Δλ/2)
 * 
 * @author Jash Sayani 
 */

public class GeoPoint {
	
	private long latitude;
	private long longitude;

	public GeoPoint()
	{
		latitude = 0;
		longitude = 0;
	}
	
	public GeoPoint(long latitude, long longitude)
	{
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	public long getLatitude() {
		return latitude;
	}

	public void setLatitude(long latitude) {
		this.latitude = latitude;
	}

	public long getLongitude() {
		return longitude;
	}

	public void setLongitude(long longitude) {
		this.longitude = longitude;
	}
	
	public float getMiles()
	{
		// Use: http://code.google.com/p/simplelatlng/
		return 0.0f;
	}
	
	public float getKilometers()
	{
		// Use: http://code.google.com/p/simplelatlng/
		return 0.0f;
	}
}
