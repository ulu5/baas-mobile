package com.baasmobile.beans;

import com.google.code.morphia.annotations.Entity;

@Entity("App")
public class App
{
	private String appname;
	private String apikey;
	private String ownerclientid;
	private String kpikey;
	private String statskey;
	
	public App()
	{
		this.appname = "";
		this.apikey = "";
		this.ownerclientid = "";
		this.kpikey = "";
		this.statskey = "";
	}
	
	public App(String appname, String apikey, String ownerclientid, String kpikey, String statskey)
	{
		this.appname = appname;
		this.apikey = apikey;
		this.ownerclientid = ownerclientid;
		this.kpikey = kpikey;
		this.statskey = statskey;
	}
	
	public String getAppname() {
		return appname;
	}

	public void setAppname(String appname) {
		this.appname = appname;
	}

	public String getApikey() {
		return apikey;
	}

	public void setApikey(String apikey) {
		this.apikey = apikey;
	}

	public String getOwnerclientid() {
		return ownerclientid;
	}

	public void setOwnerclientid(String ownerclientid) {
		this.ownerclientid = ownerclientid;
	}

	public String getKpikey() {
		return kpikey;
	}

	public void setKpikey(String kpikey) {
		this.kpikey = kpikey;
	}

	public String getStatskey() {
		return statskey;
	}

	public void setStatskey(String statskey) {
		this.statskey = statskey;
	}
	
	public void toJSON() {
		
		// Will return JSON of object 
	}
}
