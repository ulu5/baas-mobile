package com.baasmobile.beans;

import com.google.code.morphia.annotations.Entity;

@Entity("User")
public class User
{
	private String clientid;
	private String firstname;
	private String lastname;
	private String email;
	private String password;
	private String cclast4;
	private String cctype;
	private App[] apps;
	private String stripetoken;
	
	public User() {
		
		this.clientid = "";
		this.firstname = "";
		this.lastname = "";
		this.email = "";
		this.password = "";
		this.cclast4 = "";
		this.cctype = "";
		this.apps = null;
		this.stripetoken = "";
	}
	
	public User(String clientid, String firstname, String lastname, String email, String password, String cclast4, String cctype, App[] apps, String stripetoken) {
		
		this.clientid = clientid;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.password = password;
		this.cclast4 = cclast4;
		this.cctype = cctype;
		this.apps = apps;
		this.stripetoken = stripetoken;
	}

	public String getClientid() {
		return clientid;
	}

	public void setClientid(String clientid) {
		this.clientid = clientid;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setLast4(String last4) {
		this.cclast4 = last4; 
	}
	
	public String getLast4() {
		return cclast4;
	}

	public String getCardType() {
		return cctype;
	}

	public void setCardType(String type) {
		this.cctype = type;
	}

	public App[] getApps() {
		return apps;
	}

	public void setApps(App[] apps) {
		this.apps = apps;
	}

	public String getStripetoken() {
		return stripetoken;
	}

	public void setStripetoken(String stripetoken) {
		this.stripetoken = stripetoken;
	}
	
	public void toJSON() {
		
		// Will return JSON of object 
	}
	
}
