package com.baasmobile.api;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import com.baasmobile.util.Globals;
import com.baasmobile.util.Utils;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;


/**
 * Handles all API requests to add apps, delete apps and reset API Keys.
 * This class assumes that all requests are coming from the local machine
 * and not over the network. No checks are made to ensure that the correct
 * application is calling these functions.
 * 
 * @author Andrew Taeoalii
 *
 */
@Path("/app")
public class AppAPI
{	
	@Context HttpHeaders requestHeaders;
	
	/**
	 * Create an app from a form. The name stored in the database contains the 
	 * client id and the app name specified in the form.
	 * @param name - name of the app
	 * @param client id - id of the client
	 * @return
	 * @throws Exception
	 */
	@POST
	public Response createApp(@FormParam("name") String appName,
			@FormParam("clientid") String clientId) throws Exception 
	{
		try
		{
			MongoClient mongoClient = Utils.getAuthenticatedClient();

			String origAppName = appName;
			appName = clientId + appName;
		
			List<String> existingDBs = (List<String>) mongoClient.getDatabaseNames();
		
			// create database
			if(existingDBs.contains(appName))
				return Utils.unsuccessfulLocalResponse(appName + " already exists!", getRequestOrigin());
			
			createEmptyDB(mongoClient, appName);

			// get new api key
			String apikey = Utils.generateAPIKey();
		
			// create record in apps table
			DBCollection all_apps = mongoClient.getDB(Globals.SYSTEM_DB_NAME).getCollection(Globals.APPS_COLL_NAME);
			DBObject app_record = new BasicDBObject(Globals.CLIENT_ID, clientId);
			app_record.put("appname", appName);
			app_record.put(Globals.APIKEY_FIELD_NAME, apikey);
			app_record.put(Globals.APICOUNT_FIELD_NAME, new BasicDBObject());
			app_record.put(Globals.SMSCOUNT_FIELD_NAME, new BasicDBObject());
		
			// create valid
			app_record.put(Globals.VALID_RECORD, 1);
		
			// insert
			all_apps.insert(app_record);
		
			// add to array of app names in the users table
			DBCollection users = mongoClient.getDB(Globals.SYSTEM_DB_NAME).getCollection(Globals.USERS_COLL_NAME);
			BasicDBObject userQuery = new BasicDBObject();
	    		userQuery.put( Globals.CLIENT_ID, clientId );
	    		BasicDBObject updateCommand = new BasicDBObject();
	    		updateCommand.put( "$push", new BasicDBObject(Globals.APPS_COLL_NAME, origAppName));
		    	users.update(userQuery, updateCommand);
		
			// append success and return
			app_record.put("success", 1);
			
			mongoClient.close();
		
			return Utils.getResponseLocalAuth(app_record, getRequestOrigin());
		
		}
		catch (Exception e)
		{
			e.printStackTrace();
			if(Globals.production)
				return Utils.unsuccessfulAllResponse("Please try again later!");
			else
				return Utils.unsuccessfulAllResponse(e.toString());
		}
	}
	

	/**
	 * Get the api count by day, the SMS calls by day, and KPI data.
	 * 
	 * @param apiKey
	 * @return
	 */
	@GET
	public Response getAppStats(@QueryParam("apikey") String apiKey,
			@QueryParam("appname") String appName,
			@QueryParam("clientid") String clientId)
	{
		// TODO: change apikey to headerparam
		try
		{
			MongoClient mc = Utils.getAuthenticatedClient();
			
			DBCollection apps = mc.getDB(Globals.SYSTEM_DB_NAME).getCollection(Globals.APPS_COLL_NAME);
			
			DBObject query = new BasicDBObject(Globals.APIKEY_FIELD_NAME, apiKey);
			query.put("appname", clientId + appName);
			
			DBObject myApp = apps.findOne(query);
			
			// TODO: decide on KPI call
			DBCursor users = mc.getDB(clientId + appName).getCollection(Globals.KPI_COLL_NAME).find();
			myApp.put("KPI", users);
			String retVal = JSON.serialize(myApp);
			mc.close();
			
			return Utils.getResponseLocalAuth(retVal, getRequestOrigin());
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(Globals.production)
				return Utils.unsuccessfulAllResponse("Please try again later!");
			else
				return Utils.unsuccessfulAllResponse(e.toString());
		}
	}
	
	
	
	/**
	 * Used when the api key has been compromised. A new unique api key is generated
	 * for the use by the developer.
	 * @return
	 */
	@PUT
 	public Response resetAPIKey(
			@HeaderParam("clientid") String clientId,
			@HeaderParam("appname") String appName,
			@HeaderParam("apikey") String apiKey) 
	{	
		try
		{
			if(clientId==null || appName==null || apiKey==null)
				return Utils.unsuccessfulLocalResponse("Missing header params. Expected: clientid, appname, apikey!", getRequestOrigin());
			
			boolean valid = Utils.validateAPIKey(clientId, appName, apiKey);

			if (!valid)
				return Utils.unsuccessfulLocalResponse("Invalid apikey/clientid/appname!", getRequestOrigin());
			
			String newKey = Utils.generateAPIKey();
			MongoClient mc = Utils.getAuthenticatedClient();
			
			BasicDBObject query = new BasicDBObject("appname", clientId+appName);
			BasicDBObject update = new BasicDBObject(Globals.APIKEY_FIELD_NAME, newKey);

			DBCollection dbc = mc.getDB(Globals.SYSTEM_DB_NAME).getCollection(Globals.APPS_COLL_NAME);
			DBObject dbo = dbc.find(query).next();
			
			if(dbo==null || dbo.get(Globals.VALID_RECORD).equals(0))
				return Utils.unsuccessfulLocalResponse("Bad query!", getRequestOrigin());

			BasicDBObject set = new BasicDBObject("$set", update);
			dbc.update(query, set);
			
			String message = "The new apikey generated for " + appName + " is " + newKey + "!";
			
			mc.close();
			
			return Utils.successfulLocalResponse(message, getRequestOrigin());
			
		} catch (Exception e)
		{
			e.printStackTrace();
			if(Globals.production)
				return Utils.unsuccessfulAllResponse("Please try again later!");
			else
				return Utils.unsuccessfulAllResponse(e.toString());
		}
		
		
	}
	
	/**
	 * App becomes invalid in the apps table. All info is kept, but a single
	 * field invalidates it.
	 * @param appName
	 * @param clientId
	 * @return
	 * @throws Exception
	 */
	@DELETE
	public Response removeApp(
			@HeaderParam("appname") String appName,
			@HeaderParam("clientid") String clientId,
			@HeaderParam("apikey") String apiKey) throws Exception 
	{
		try
		{
			boolean valid = Utils.validateAPIKey(clientId, appName, apiKey);

			if (!valid)
			{
				BasicDBObject dbo = new BasicDBObject(appName, "not removed!");
				dbo.append("success", 0);
				return Utils.getResponseLocalAuth(dbo, getRequestOrigin());
			}
		
			MongoClient mongoClient = Utils.getAuthenticatedClient();
		
			// invalidate record in apps table
			DBCollection all_apps = mongoClient.getDB(Globals.SYSTEM_DB_NAME).getCollection(Globals.APPS_COLL_NAME);
		
			BasicDBObject query = new BasicDBObject("appname", clientId + appName);
			BasicDBObject set = new BasicDBObject("$set", new BasicDBObject(Globals.VALID_RECORD, 0));
			all_apps.update(query, set);
		
		
			// remove from users table
			DBCollection users = mongoClient.getDB(Globals.SYSTEM_DB_NAME).getCollection(Globals.USERS_COLL_NAME);
			BasicDBObject userQuery = new BasicDBObject(Globals.CLIENT_ID, clientId);
			BasicDBObject removeApp = new BasicDBObject("$pull", new BasicDBObject(Globals.APPS_COLL_NAME, appName));
			users.update(userQuery, removeApp); 
		
			mongoClient.close();
			
			return Utils.successfulLocalResponse(appName + " removed successfully!", getRequestOrigin());
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(Globals.production)
				return Utils.unsuccessfulAllResponse("Please try again later!");
			else
				return Utils.unsuccessfulAllResponse(e.toString());		
		}
		
	}
	

	/**
	 * Create an empty database for a user.
	 * @param mongoClient
	 * @param name
	 * @return
	 */
	private DB createEmptyDB(MongoClient mongoClient, String name)
	{
		DB myNewDB = mongoClient.getDB(name);
		myNewDB.createCollection("new_coll", null);
		myNewDB.getCollection("new_coll").drop();
		
		return myNewDB;
	}
	
	/**
	 * Pulls origin out of the headers.
	 * @return
	 * @throws Exception
	 */
	private String getRequestOrigin() throws Exception
	{	
		MultivaluedMap<String, String> myMap = requestHeaders.getRequestHeaders();
		if(myMap.containsKey("origin"))
			return myMap.get("origin").get(0);
		else if(myMap.containsKey("Origin"))
			return myMap.get("Origin").get(0);
		else
			throw new Exception("Origin header not found!");
	}
	
	/**
	 * Listens for an OPTIONS request from a user-agent. Approves
	 * the requested method (PUT/DELETE)
	 *  
	 */
	@OPTIONS
	public Response approveOptions()
	{
		return Utils.getResponseAllAuth("options msg");
	}
	
}
