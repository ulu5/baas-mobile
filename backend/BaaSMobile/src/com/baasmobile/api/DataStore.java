package com.baasmobile.api;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.baasmobile.util.Utils;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.util.JSON;

@Path("/data")
public class DataStore 
{
	
	@Context HttpHeaders requestHeaders;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getCollection()
	{
		DB db;
		try 
		{
			db = Utils.getAuthenticatedClient().getDB("gridtest");
			DBCollection coll = db.getCollection("test");
			return Utils.getResponseAllAuth(JSON.serialize(coll.find()));
		} catch (Exception e) 
		{
			e.printStackTrace();
		}	
		return null;
	}
}
