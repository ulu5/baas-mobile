package com.baasmobile.api;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.DELETE;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;

import com.baasmobile.util.Globals;
import com.baasmobile.util.Utils;
import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

@Path("/user/{clientid}")
public class AccountMod
{
	@Context HttpHeaders requestHeaders;
	@Context HttpServletRequest request;

	/**
	 * Update user's information according to the email given in the path.
	 * 
	 * @param email
	 * @param password
	 * @param firstname
	 * @param lastname
	 * @return
	 */
	@PUT
	public Response updateuser(@PathParam("clientid")String clientid, 
			@HeaderParam("email")String email,
			@HeaderParam("password")String password, 
			@HeaderParam("firstname")String firstname, 
			@HeaderParam("lastname")String lastname) 
	{
		try
		{
			MongoClient mc = Utils.getAuthenticatedClient();
			
			BasicDBObject query = new BasicDBObject(Globals.CLIENT_ID, clientid);
			
			DBCollection dbc = mc.getDB(Globals.SYSTEM_DB_NAME).getCollection(Globals.USERS_COLL_NAME);
			DBObject user = dbc.find(query).next();
			
			if(user==null || user.get(Globals.VALID_RECORD).equals(0))
				return Utils.getResponseLocalAuth("User not found!", getRequestOrigin());
			
			BasicDBObject update = new BasicDBObject();
			if(email!=null)
			{
				update.append("email", email);
			}
			if(password!=null)
			{
				update.append("password", password);
			}
			if(firstname!=null)
			{
				update.append("firstname", firstname);
			}
			if(lastname!=null)
			{
				update.append("lastname", lastname);
			}

			BasicDBObject set = new BasicDBObject("$set", update);
			dbc.update(query, set);
			
			String updateMsg = "Updated record " + clientid + " with " + update.toString();
			mc.close();
			return Utils.successfulLocalResponse(updateMsg, getRequestOrigin());
			
		} catch (Exception e)
		{
			e.printStackTrace();
			if(Globals.production)
				return Utils.unsuccessfulAllResponse("Please try again later!");
			else
				return Utils.unsuccessfulAllResponse(e.toString());	
		}
	}

	/**
	 * Closes the account of a user.
	 * 
	 * @param clientId
	 * @return
	 */
	@DELETE
	public Response closeacct(@PathParam("clientid")String clientid)  
	{
		try
		{
			MongoClient mongoClient = Utils.getAuthenticatedClient();
			
			DBCollection users = mongoClient.getDB(Globals.SYSTEM_DB_NAME).getCollection(Globals.USERS_COLL_NAME);
			BasicDBObject query = new BasicDBObject(Globals.CLIENT_ID, clientid);
			
			// invalidate user
			BasicDBObject set = new BasicDBObject("$set", new BasicDBObject(Globals.VALID_RECORD, 0));
			users.update(query, set);
			
			// invalidate all of user's apps
			DBCollection all_apps = mongoClient.getDB(Globals.SYSTEM_DB_NAME).getCollection(Globals.APPS_COLL_NAME);
			BasicDBObject set1 = new BasicDBObject("$set", new BasicDBObject(Globals.VALID_RECORD, 0));
			all_apps.updateMulti(query, set1);
			String deletedMsg = "deleted account - " + clientid;
			mongoClient.close();
			return Utils.successfulLocalResponse(deletedMsg, getRequestOrigin());
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(Globals.production)
				return Utils.unsuccessfulAllResponse("Please try again later!");
			else
				return Utils.unsuccessfulAllResponse(e.toString());	
		}
	}
	
	/**
	 * An OPTIONS request must be here to ensure that PUT and DELETE methods work.
	 * @return
	 */
	@OPTIONS
	public Response options() 
	{
		return Utils.getResponseAllAuth("options msg");
  }
	  
	private String getRequestOrigin()
	{
		return requestHeaders.getRequestHeader("origin").get(0);
	}
}
