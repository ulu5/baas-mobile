package com.baasmobile.api;

import java.util.ArrayList;
import java.util.regex.*;


import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.servlet.http.HttpServletRequest;


import org.bson.types.ObjectId;

import com.baasmobile.beans.User;
import com.baasmobile.plugins.MailGun;
import com.baasmobile.util.Globals;
import com.baasmobile.util.Utils;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.util.JSON;

/**
 * This class provides a RESTful user sign-up and authentication service 
 * for client-side authentication for visitors signing into the website.
 * 
 * Provides functionality for:
 * 	1. User sign-up
 * 	2. User login authentication
 * 	3. User account updates
 * 	4. User account deletion
 * 
 *
 */

@Path("/user")
public class Account 
{

	@Context HttpHeaders requestHeaders;
	@Context HttpServletRequest request;
	
	/**
	 * Login
	 * @param email
	 * @param pwd
	 * @return
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response login(@QueryParam("email") String email, @QueryParam("password") String pwd) 
	{
		try
		{
			DBObject user = getUser(email, pwd);
			DBObject response = new BasicDBObject();
			String origin = getRequestOrigin();
			
			if(user==null)
			{
				response.put("success", 0);
				response.put("errmsg", "Email not in use, null!");
				return Utils.getResponseLocalAuth(response, origin);
			}
			
			if(user.containsField("error"))
			{
				if(Globals.production)
					return Utils.unsuccessfulAllResponse("Please try again later!");
				else
					return Utils.unsuccessfulLocalResponse(user.toString(), origin);
			}
		
			// user doesn't exist or has been deleted
			if(user.get(Globals.VALID_RECORD).equals(0))
			{
				return Utils.unsuccessfulLocalResponse("Email not in use!", origin);
			}
			else if(user.get("password").equals(pwd))
			{
				updateLoginCols(false, email);	// Update the login date, and login IP address
				user.removeField(Globals.VALID_RECORD);
				response.put("success", 1);
				
				ArrayList<DBObject> usersApps = Utils.getAppsAndTables(user);
				
				user.put(Globals.APPS_COLL_NAME, usersApps);
				response.put("user", user);	
			}
			else
			{
				return Utils.unsuccessfulLocalResponse("Password/Email do not match!", origin);
			}
			
			return Utils.getResponseLocalAuth(response, origin);
		}
		catch(Exception e)
		{
			e.printStackTrace();
			if(Globals.production)
				return Utils.unsuccessfulAllResponse("Please try again later!");
			else
				return Utils.unsuccessfulAllResponse(e.toString());	
		}

	}
	
	/**
	 * Sign up form.
	 * 
	 * @param email
	 * @param first
	 * @param last
	 * @param pwd
	 * @return
	 * @throws Exception
	 */
	@POST
	public Response signup(@FormParam("email") String email, 
			@FormParam("firstname") String first, 
			@FormParam("lastname") String last, 
			@FormParam("password") String pwd,
			@FormParam("plan") String plan,
			MultivaluedMap<String, String> form) throws Exception 
	{
		try 
		{	
			
			// Email regular expression check:
			Pattern emailPattern = Pattern.compile("([^.@]+)(\\.[^.@]+)*@([^.@]+\\.)+([^.@]+)");
			Matcher matcher = emailPattern.matcher(email);
			if(!matcher.matches())
				return Utils.unsuccessfulLocalResponse("Invalid email address!", getRequestOrigin());
			
			// authenticate
			MongoClient mc = Utils.getAuthenticatedClient();
			DB db = mc.getDB(Globals.SYSTEM_DB_NAME);
			
			DBCollection users = db.getCollection(Globals.USERS_COLL_NAME);
			BasicDBObject newUser = new BasicDBObject("email", email);
			DBCursor dbc = users.find(newUser);
			
			// check email exists
			if(dbc.hasNext())
				return Utils.unsuccessfulLocalResponse("Email already exists!", getRequestOrigin());
			
			// TODO: email verification needed? send email to verify its valid and in use
			MailGun.sendWelcomeEmail(email, first);
			
			ObjectId oid = new ObjectId();
			
			if(plan == null || plan.equals(Globals.FREE_PLAN))
				plan = Globals.FREE_PLAN;
			else
			{
				String token = form.getFirst("token");
				User user = Utils.createStripeCustomer(token, email, plan);
				
				newUser.append("cardtype", user.getCardType());
				newUser.append("cardnumber", user.getLast4());
				newUser.append("stripetoken", user.getStripetoken());
			}
			
			newUser.append("password", pwd).
					append("firstname", first).
					append("lastname", last).
					append("plan", plan).
					append("_id", oid).
					append(Globals.CLIENT_ID, Utils.getClientId(oid.toString()));
			
			// enable user
			newUser.append(Globals.VALID_RECORD, 1);
			
			// add empty stripeconnect and empty apps array
			newUser.append(Globals.APPS_COLL_NAME, new ArrayList<String>());
			newUser.append("stripeconnect", new ArrayList<String>());
			
			
			users.insert(newUser);
			
			// Log the signupdate, lastlogindate and lastloginip
			updateLoginCols(true, email);
			
			DBObject user = users.findOne(newUser);
			user.put("success", 1);
			
			String retval = JSON.serialize(user);
			
			mc.close();
			
			return Utils.getResponseLocalAuth(retval, getRequestOrigin());
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			if(Globals.production)
				return Utils.unsuccessfulAllResponse("Please try again later!");
			else
				return Utils.unsuccessfulAllResponse(e.toString());	
		}
	}

	
	/**
	 * Function that verifies user credentials against list of
	 * registered CloudHub users in system db users list.
	 * @param user
	 * @param pwd
	 * @return
	 */
	private DBObject getUser(String user, String pwd) 
	{
		try 
		{
			MongoClient mc = Utils.getAuthenticatedClient();
			DB db = mc.getDB(Globals.SYSTEM_DB_NAME);
			DBCollection users = db.getCollection(Globals.USERS_COLL_NAME);
	        BasicDBObject query = new BasicDBObject("email", user);
	        DBCursor cursor = users.find(query);
	        if(cursor.hasNext())
	        	return cursor.next();
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return new BasicDBObject("error", e.toString());
		}
		return null;
		
	}
	
	/**
	 * Updates the user record specified by "email" with the current
	 * login date (unix time stamp) and the current login ip of the client.
	 * @param email
	 * @return
	 */
	private boolean updateLoginCols(boolean signup, String email) 
	{
		
		MongoClient mc;
		try {
			mc = Utils.getAuthenticatedClient();
			BasicDBObject query = new BasicDBObject("email", email);
			DBCollection dbc = mc.getDB(Globals.SYSTEM_DB_NAME).getCollection(Globals.USERS_COLL_NAME);
			BasicDBObject update = new BasicDBObject();
			
			// Log the unix timestamp of this successful login attempt
			long unixTime = System.currentTimeMillis() / 1000L;
			update.put("lastlogindate", unixTime);
			
			// If this is a signup, signupdate = lastlogindate
			if(signup) {
				update.put("signupdate", unixTime);
			}
			
			// Get the IP address
			String ip = request.getRemoteAddr();
			update.put("lastloginip", ip);
			
			BasicDBObject set = new BasicDBObject("$set", update);
			dbc.update(query, set);
			mc.close();
			return true;			
			
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
			return false;
		}

		
	}
	
	/**
	 * Pulls origin out of the headers.
	 * @return
	 * @throws Exception
	 */
	private String getRequestOrigin() throws Exception
	{	
		MultivaluedMap<String, String> myMap = requestHeaders.getRequestHeaders();
		if(myMap.containsKey("origin"))
			return myMap.get("origin").get(0);
		else if(myMap.containsKey("Origin"))
			return myMap.get("Origin").get(0);
		else
			throw new Exception("Origin header not found!");
	}
	
}
