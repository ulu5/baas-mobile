package com.baasmobile.api;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;

import com.baasmobile.beans.BaaSMobileStream;
import com.baasmobile.util.Globals;
import com.baasmobile.util.Utils;
import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSDBFile;

@Path("/files")
public class FileRetrieve
{
	private static String DEFAULT_IMAGE = "notfound.gif";
	
	@GET
	@Path("/{clientid}/{appname}/{filename}")
	public Response getIsoFile(@PathParam("filename") String filename,
			@PathParam("clientid") String clientId,
			@PathParam("appname") String appName)
	{
		return getFile(clientId, appName, filename);
	}
	
	@GET
	@Path("/image/{clientid}/{appname}/{filename}")
	@Produces({"image/gif, image/jpeg"})
	public Response getImageFile(@PathParam("filename") String filename,
			@PathParam("clientid") String clientId,
			@PathParam("appname") String appName)
	{
		return getFile(clientId, appName, filename);
	}
	
	@GET
	@Path("/audio/{clientid}/{appname}/{filename}")
	@Produces({"audio/wav, audio/mpeg"})
	public Response getAudioFile(@PathParam("filename") String filename,
			@PathParam("clientid") String clientId,
			@PathParam("appname") String appName)
	{
		return getFile(clientId, appName, filename);
	}
	
	@GET
	@Path("/video/{clientid}/{appname}/{filename}")
	@Produces({"video/mp4", "video/x-quicktime"})
	public Response getVideoFile(@PathParam("filename") String filename,
			@PathParam("clientid") String clientId,
			@PathParam("appname") String appName)
	{
		return getFile(clientId, appName, filename);
	}

	@DELETE
	@Path("/{clientid}/{appname}/{filename}")
	public Response deleteFile(@PathParam("filename") String filename,
			@PathParam("clientid") String clientId,
			@PathParam("appname") String appName,
			@HeaderParam("apikey") String apiKey)
	{
		try
		{
			DB db = Utils.getAuthenticatedClient().getDB(clientId+appName + Globals.FILES);

			GridFS filestoreGrid = new GridFS(db, Globals.STORAGE);
			filestoreGrid.remove(filename);
			return Utils.successfulAllResponse("Removed '" + filename + "' successfully!\n");
		} catch (Exception e)
		{
			e.printStackTrace();
			if(Globals.production)
				return Utils.unsuccessfulAllResponse("Please try again later!");
			else
				return Utils.unsuccessfulAllResponse(e.toString());
		}
	}
	
	/**
	 * Helper function to get the file with the specific name of the file, appname,
	 * and client id. Stores in the grid with the clientid+appname.
	 * 
	 * @param clientId
	 * @param appName
	 * @param apiKey
	 * @param filename
	 * @return
	 */
	private Response getFile(String clientId, String appName, String filename)
	{
		try
		{
			MongoClient mc = Utils.getAuthenticatedClient();
			DB db = mc.getDB(clientId+appName + Globals.FILES);
			
			GridFS filestoreGrid = new GridFS(db, Globals.STORAGE);
			GridFSDBFile imageForOutput = filestoreGrid.findOne(filename);
			
			if(imageForOutput==null)
			{
				imageForOutput = filestoreGrid.findOne(DEFAULT_IMAGE);
				if(imageForOutput==null)
				{
					throw new Exception("Not found image not found!");
				}
			}
			StreamingOutput stream = new BaaSMobileStream(imageForOutput);
			
			Response retval = Response.ok(stream).build();
			
			return retval;
		} catch (Exception e)
		{
			e.printStackTrace();
			if(Globals.production)
				return Utils.unsuccessfulAllResponse("Please try again later!");
			else
				return Utils.unsuccessfulAllResponse(e.toString());
		}
	}
	
}
