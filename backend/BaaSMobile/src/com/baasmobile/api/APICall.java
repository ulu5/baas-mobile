package com.baasmobile.api;

import java.io.InputStream;
import java.net.UnknownHostException;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.bson.types.ObjectId;

import com.baasmobile.util.CountUtil;
import com.baasmobile.util.Globals;
import com.baasmobile.util.Utils;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.gridfs.GridFS;
import com.mongodb.gridfs.GridFSInputFile;
import com.mongodb.util.JSON;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;

/**
 * Handles all calls that are from remote connections. 
 * 
 *
 */
@Path("/v1/{clientid}/{appname}/{tablename}")
public class APICall 
{ 
	@Context HttpHeaders requestHeaders;
	
	/**
	 * Make a get request to read from a table. You may use a query to get certain
	 * objects from the database.
	 * 
	 * 
	 * This returns a default limit of 100 records as specified in Globals.
	 * 
	 * @param clientId
	 * @param appName
	 * @param dataName
	 * @return
	 * @throws Exception 
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getRecord(
			@PathParam("clientid")String clientId,
			@PathParam("appname")String appName,
			@PathParam("tablename")String tableName,
			@QueryParam("login") String login,
			@Context UriInfo uriInfo) throws Exception
	{
		try 
		{	
			// TODO: change apikey to headerparam
			MultivaluedMap<String, String> queryParams = uriInfo.getQueryParameters();
			String apikey = queryParams.getFirst(Globals.APIKEY_FIELD_NAME);
			
			
			if(!Utils.validateAPIKey(clientId, appName, apikey))
			{
				
				return Utils.unsuccessfulAllResponse("Invalid api key/client id! apikey:" + apikey + " clientid: " + clientId);
			}
			
			int startingAt = 0;
			int requestLimit = Globals.API_LIMIT;
			
			if(queryParams.containsKey("startindex"))
			{
				startingAt = Integer.parseInt(queryParams.get("startindex").get(0));
			}
			
			if(queryParams.containsKey("limit"))
			{
				requestLimit = Integer.parseInt(queryParams.get("limit").get(0));
			}
			
			queryParams.remove("limit");
			queryParams.remove("startindex");
			queryParams.remove(Globals.APIKEY_FIELD_NAME);
			
			BasicDBObject queryObject;
			
			if(queryParams.containsKey("query"))
			{
				DBObject query = (DBObject) JSON.parse(queryParams.getFirst("query"));
				queryParams.remove("query");
				
				queryObject = Utils.convertMapToDBObject(queryParams);
				queryObject.putAll(query);
				
			}
			else
			{
				queryObject = Utils.convertMapToDBObject(queryParams);
				
			}
			
			
			// is this a valid record
			queryObject.append(Globals.VALID_RECORD, 1);
			
			MongoClient mongoClient = Utils.getAuthenticatedClient();
			DB appDB = mongoClient.getDB(clientId + appName);
			
			if(tableName.equals(Globals.USERS_COLL_NAME) && login.equalsIgnoreCase("true"))
			{
				DBCollection collection = appDB.getCollection(tableName);
				DBObject dbo = collection.findOne(queryObject);
				
				// update last login time
				long unixTime = System.currentTimeMillis() / 1000L;
				BasicDBObject update = new BasicDBObject("lastlogin", unixTime);
				
				BasicDBObject set = new BasicDBObject("$set", update);
				collection.update(queryObject, set);
				String retVal = JSON.serialize(dbo);
				CountUtil.incrementTodaysApiCount(apikey, mongoClient);
				mongoClient.close();
				return Utils.getResponseAllAuth(retVal);
			}
			else
			{
				DBCursor dbc;
				
				if(queryParams.containsKey("sort"))
				{
					queryObject.remove("sort");
					DBCollection collection = appDB.getCollection(tableName);
					DBObject sorter = (DBObject) JSON.parse(queryParams.getFirst("sort"));
					dbc = collection.find(queryObject).limit(requestLimit).skip(startingAt).sort(sorter);
				}
				else
				{
					DBCollection collection = appDB.getCollection(tableName);
					dbc = collection.find(queryObject).limit(requestLimit).skip(startingAt);
				}

				String retVal = JSON.serialize(dbc);
				// TODO: remove valid table bit for each record
				CountUtil.incrementTodaysApiCount(apikey, mongoClient);
				mongoClient.close();
				return Utils.getResponseAllAuth(retVal);
			}
			
		} 
		catch (UnknownHostException e) 
		{
			e.printStackTrace();
			if(Globals.production)
				return Utils.unsuccessfulAllResponse("Please try again later!");
			else
				return Utils.unsuccessfulAllResponse(e.toString());	
		}
	}

	/**
	 * Post request. This adds a record to the app specified. The request is made with the
	 * following template.
	 * 
	 * 
	 * @param clientId
	 * @param appName
	 * @param dataName
	 * @return
	 * @throws Exception 
	 */
	@POST
	public Response addRecord(
			@PathParam("clientid")String clientId,
			@PathParam("appname")String appName,
			@PathParam("tablename")String tableName,
			MultivaluedMap<String, String> form) throws Exception
	{
		try 
		{	
			String apiKey = form.getFirst(Globals.APIKEY_FIELD_NAME);
	
			form.remove(Globals.APIKEY_FIELD_NAME);
			
			if(!Utils.validateAPIKey(clientId, appName, apiKey))
				return Utils.unsuccessfulAllResponse("Invalid api key/client id!");
			
			BasicDBObject newRecord = Utils.convertMapToDBObject(form);
			newRecord.append(Globals.VALID_RECORD, 1);
			
			MongoClient mongoClient = Utils.getAuthenticatedClient();
			
			DB appDB = mongoClient.getDB(clientId + appName);
			DBCollection retData = appDB.getCollection(tableName);
			
			// append signupdate if its a post to users table
			if(tableName.equals(Globals.USERS_COLL_NAME))
			{
				long unixTime = System.currentTimeMillis() / 1000L;
				newRecord.append("signupdate", unixTime);
			}
			
			retData.insert(newRecord);
			CountUtil.incrementTodaysApiCount(apiKey, mongoClient);
			String retObj = retData.find(newRecord).toArray().toString();
			mongoClient.close();
			return Utils.getResponseAllAuth(retObj);
			
		} 
		catch (UnknownHostException e) 
		{
			e.printStackTrace();
			if(Globals.production)
				return Utils.unsuccessfulAllResponse("Please try again later!");
			else
				return Utils.getResponseAllAuth(e.toString());
		}
	}
	
	/**
	 * Adds a file.
	 * 
	 * @param clientId
	 * @param appName
	 * @param tableName
	 * @param fileStream
	 * @param fileDetail
	 * @param apiKey
	 * @return
	 */
	@POST
	@Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response uploadFile(@PathParam("clientid")String clientId,
			@PathParam("appname")String appName,
			@PathParam("tablename")String tableName,
			@FormDataParam("file") InputStream fileStream,
	        @FormDataParam("file") FormDataContentDisposition fileDetail,
	        @FormDataParam("apikey") String apiKey)
	{
		try
		{
			MongoClient mongoClient = Utils.getAuthenticatedClient();
			DB db = mongoClient.getDB(clientId+appName+Globals.FILES);
			
			if(!Utils.validateAPIKey(clientId, appName, apiKey))
				return Utils.unsuccessfulAllResponse("Invalid api key/client id!");

			ObjectId filename = new ObjectId();
			GridFS filestoreGrid = new GridFS(db, Globals.STORAGE);
			GridFSInputFile gfsFile = filestoreGrid.createFile(fileStream);
			gfsFile.setFilename(filename.toString());
			gfsFile.save();
			
			BasicDBObject retVal = new BasicDBObject("success", 1);
			retVal.append("url", Globals.FILES_URL + clientId + "/" + appName + "/" + filename);
			CountUtil.incrementTodaysApiCount(apiKey, mongoClient);
			mongoClient.close();
			return Utils.getResponseAllAuth(retVal);
		} catch (Exception e)
		{
			e.printStackTrace();
			if(Globals.production)
				return Utils.unsuccessfulAllResponse("Please try again later!");
			else
				return Utils.unsuccessfulAllResponse(e.toString());		
		}
	}
	
	
	/**
	 * Make a put request to update a record in the user's table.
	 * 
	 * @param clientId
	 * @param appName
	 * @param tableName
	 * @return
	 * @throws Exception 
	 */
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateRecord(
			@PathParam("clientid")String clientId,
			@PathParam("appname")String appName,
			@PathParam("tablename")String tableName, 
			@HeaderParam("apikey") String apiKey,
			@HeaderParam("query") String query,
			@HeaderParam("update") String update) throws Exception
	{
		try 
		{	
			if(!Utils.validateAPIKey(clientId, appName, apiKey))
				return Utils.unsuccessfulAllResponse("Invalid api key/client id!");
			MongoClient mc = Utils.getAuthenticatedClient();
			DB appDB = mc.getDB(clientId + appName);
			DBCollection table = appDB.getCollection(tableName);
			
			DBObject myquery = (DBObject) JSON.parse(query);
			DBObject myupdate = (DBObject) JSON.parse(update);

			DBObject set = new BasicDBObject("$set", myupdate);
			
			table.updateMulti(myquery, set);
			String retObj = JSON.serialize(table.find(myupdate));
			CountUtil.incrementTodaysApiCount(apiKey, mc);
			mc.close();
			return Utils.getResponseAllAuth(retObj);
			
		} 
		catch (UnknownHostException e) 
		{
			e.printStackTrace();
			if(Globals.production)
				return Utils.unsuccessfulAllResponse("Please try again later!");
			else
				return Utils.unsuccessfulAllResponse(e.toString());	
		}
	}
	
	
	/**
	 * Make a delete request to delete from a table.
	 * 
	 * @param clientId
	 * @param appName
	 * @param dataName
	 * @return
	 * @throws Exception 
	 */
	@DELETE
	public Response deleteRecord(
			@PathParam("clientid")String clientId,
			@PathParam("appname")String appName,
			@PathParam("tablename")String tableName, 
			@HeaderParam("apikey") String apiKey,
			@HeaderParam("query") String query) throws Exception
	{
		try 
		{
			if(!Utils.validateAPIKey(clientId, appName, apiKey))
				return Utils.unsuccessfulAllResponse("Invalid api key/client id!");
			
			MongoClient mongoClient = Utils.getAuthenticatedClient();
			DBObject queryObject = (DBObject) JSON.parse(query);
			
			DB appDB = mongoClient.getDB(clientId + appName);
			DBCollection table = appDB.getCollection(tableName);
		
			BasicDBObject update = new BasicDBObject(Globals.VALID_RECORD, 0);
			BasicDBObject set = new BasicDBObject("$set", update);			
			table.updateMulti(queryObject, set);
			
			String msg = "The following records have been removed - " + JSON.serialize(table.find(queryObject));
			CountUtil.incrementTodaysApiCount(apiKey, mongoClient);
			mongoClient.close();
			return Utils.successfulAllResponse(msg);
	
			
		} 
		catch (UnknownHostException e) 
		{
			e.printStackTrace();
			if(Globals.production)
				return Utils.unsuccessfulAllResponse("Please try again later!");
			else
				return Utils.unsuccessfulAllResponse(e.toString());	
		}
	}
	
	/**
	 * Listens for an OPTIONS request from a user-agent. Approves
	 * the requested method (PUT/DELETE)
	 *  
	 */
	@OPTIONS
	public Response approveOptions()
	{
		return Utils.successfulAllResponse("");
	}
	
	
} 
