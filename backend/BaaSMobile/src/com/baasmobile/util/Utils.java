package com.baasmobile.util;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import java.util.Set;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.bson.types.ObjectId;

import com.baasmobile.beans.User;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.stripe.Stripe;
import com.stripe.model.Card;
import com.stripe.model.Customer;

/**
 * This class provides helper functions to be used in the project.
 * 
 *
 */
public class Utils 
{
	

	
	public static void init()
	{
		Properties prop = new Properties();
		try
		{
			if(Globals.production)
				prop.load(new FileInputStream(Globals.CONFIG_PATH + "config.properties"));
			else
			{
				System.out.println("****WARNING: NOT IN PRODUCTION*******");
				prop.load(new FileInputStream(Globals.TEST_CONFIG_PATH + "config.properties"));
			}
			Globals.MONGO_HOST = prop.getProperty("mongo_host");
			Globals.MONGO_PORT = Integer.parseInt(prop.getProperty("mongo_port"));
			Globals.MONGO_AUTH_DB = prop.getProperty("mongo_auth_db");
			Globals.MONGO_AUTH_USER = prop.getProperty("mongo_auth_user");
			Globals.MONGO_AUTH_PWD = prop.getProperty("mongo_auth_pwd");
			System.out.println("*******DONE LOADING CONFIG*******");
			
			
		} catch (FileNotFoundException e)
		{
			e.printStackTrace();
		} 
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * JInt (courtesy of Jash Sayani) - patent pending
	 * Given an ObjectId in String format, returns a unique 13 digit client id.
	 * The objectid is a 12-byte hex string. We extract the last three bytes of
	 * this id and substitute each hex digit with strings according to the following
	 * mapping:
	 * 0 = 00, 1 = 01, 2 = 02 ... A = 10, B = 11 ... F = 15
	 * The resulting 12 digit string is then appended to '1' to create a 13 digit
	 * client ID that starts with '1'.
	 */
	public static String getClientId(String objectid)
	{
		// Store mapping in an array
		String[] mapping = {"00", "01", "02", "03", "04", "05", "06", "07",
							"08", "09", "10", "11", "12", "13", "14", "15"};
		String id = "1";
	
		// Strip out the last 3 bytes from the objectid and replace it with its mapping
		for(int i = 18; i < 24; i++) {
			id += mapping[Integer.parseInt(objectid.substring(i, i+1), 16)];
		}
		return id;
	}
	
	/**
	 * Authenticates connection to mongo database and returns the mongoClient
	 * connection that has been authenticated.
	 * @return
	 * @throws Exception
	 */
	public static MongoClient getAuthenticatedClient() throws Exception
	{
		// authenticate
		MongoClient mongoClient = new MongoClient( Globals.MONGO_HOST , Globals.MONGO_PORT );
		DB db = mongoClient.getDB(Globals.MONGO_AUTH_DB);
		boolean val = db.authenticate(Globals.MONGO_AUTH_USER, Globals.MONGO_AUTH_PWD.toCharArray());
		if(!val)
			throw new Exception("invalid authentication!");
		
		
		
		return mongoClient;
	}
	

	/**
	 * Validates api key given client id and app name.
	 * 
	 * @param clientId
	 * @param appName
	 * @param apiKey
	 * @return
	 */
	public static boolean validateAPIKey(String clientId, String appName, String apiKey)
	{
		try
		{
			MongoClient mc = getAuthenticatedClient();
			DBCollection all_apps = mc.getDB(Globals.SYSTEM_DB_NAME).getCollection(Globals.APPS_COLL_NAME);
			BasicDBObject query = new BasicDBObject("appname", clientId+appName);
			query.append(Globals.VALID_RECORD, 1);
			query.append(Globals.APIKEY_FIELD_NAME, apiKey);
			DBCursor dbc = all_apps.find(query);
			boolean retval = dbc.hasNext();
			mc.close();
			return retval;
			
		} 
		catch (Exception e)
		{
			e.printStackTrace();
			return false;
		}
	}
	
	/**
	 * Generates a new API Key for the user's app.
	 * 
	 * @return new api key
	 */
	public static String generateAPIKey()
	{
		ObjectId oid = new ObjectId();
		return oid.toString();
	}
	
	/**
	 * Returns a response that allows cross-origin calls from local server only.
	 * 
	 * @param dbo
	 * @return
	 */
	public static Response getResponseLocalAuth(String message, String origin)
	{
		if(Globals.ORIGINS.contains(origin))
		{
			ResponseBuilder resp = Response.ok(message).header(Globals.CORS_ORIGINS, origin);
			resp.header(Globals.CORS_METHODS, Globals.ALLOWABLE_METHODS);
			return resp.build();
		}
		else
			return unsuccessfulAllResponse("Not Authorized!");
	}
	
	
	/**
	 * Returns a response that allows cross-origin calls from any origin.
	 * 
	 * @param dbo
	 * @return
	 */
	public static Response getResponseAllAuth(String message)
	{
		ResponseBuilder resp = Response.ok(message).header(Globals.CORS_ORIGINS, "*");
		resp.header(Globals.CORS_METHODS, Globals.ALLOWABLE_METHODS);
		resp.header(Globals.CORS_HEADERS, Globals.ALLOWABLE_HEADERS);
		return resp.build();
	}
	
	/**
	 * Returns a response that allows cross-origin calls from local server only.
	 * 
	 * @param dbo
	 * @return
	 */
	public static Response getResponseLocalAuth(DBObject dbo, String origin)
	{
		return getResponseLocalAuth(dbo.toString(), origin);
	}	
	
	/**
	 * Returns a response that allows cross-origin calls from any origin.
	 * 
	 * @param dbo
	 * @return
	 */
	public static Response getResponseAllAuth(DBObject dbo)
	{
		return getResponseAllAuth(dbo.toString());
	}
	
	/**
	 * Returns an authenticated response with a success of 0 and a message
	 * passed in as a parameter.
	 * @param message
	 * @return
	 */
	public static Response unsuccessfulLocalResponse(String message, String origin)
	{
		BasicDBObject dbo = new BasicDBObject("success", 0);
		dbo.append("errmsg", message);
		return Utils.getResponseLocalAuth(dbo, origin);
	}
	
	/**
	 * Returns an authenticated response with a success of 1 and a message
	 * passed in as a parameter.
	 * @param message
	 * @return
	 */
	public static Response successfulLocalResponse(String message, String origin)
	{
		BasicDBObject dbo = new BasicDBObject("success", 1);
		dbo.append("message", message);
		return Utils.getResponseLocalAuth(dbo, origin);
	}
	
	/**
	 * Returns an authenticated response with a success of 0 and a message
	 * passed in as a parameter.
	 * @param message
	 * @return
	 */
	public static Response unsuccessfulAllResponse(String message)
	{
		BasicDBObject dbo = new BasicDBObject("success", 0);
		dbo.append("errmsg", message);
		return Utils.getResponseAllAuth(dbo);
	}
	
	/**
	 * Returns an authenticated response with a success of 1 and a message
	 * passed in as a parameter.
	 * @param message
	 * @return
	 */
	public static Response successfulAllResponse(String message)
	{
		BasicDBObject dbo = new BasicDBObject("success", 1);
		dbo.append("message", message);
		return Utils.getResponseAllAuth(dbo);
	}
	
	/**
	 * Converts the map to a dbobject.
	 * 
	 * @param queryParams
	 * @return
	 */
	public static BasicDBObject convertMapToDBObject(MultivaluedMap<String, String> queryParams)
	{
		BasicDBObject retObject = new BasicDBObject();
		
		for(String key : queryParams.keySet())
		{
			retObject.append(key, queryParams.getFirst(key));
		}
		
		return retObject;
	}

	
	
	/**
	 * Creates a stripe customer - associates token from frontend with user email.
	 * @param token - obtained using Stripe.js in frontend
	 * @param email - from user sign up
	 * @return success
	 */
 	public static User createStripeCustomer(String token, String email, String plan) 
	{	
		HashMap<String, Object> customerParams = new HashMap<String, Object>();
		customerParams.put("card", token);
		customerParams.put("email", email);
		try {
			
			Stripe.apiKey = Globals.STRIPE_API_KEY;
			
			// Create customer
			Customer response = Customer.create(customerParams);
			String customerId = response.getId();
			Card active = response.getActiveCard();
			String last4 = active.getLast4();
			String type = active.getType();
			
			User usr = new User();
			usr.setCardType(type);
			usr.setLast4(last4);
			usr.setStripetoken(customerId);
			
			// Charge the customer
			HashMap<String, Object> subscriptionParams = new HashMap<String, Object>();
			subscriptionParams.put("plan", plan);
			subscriptionParams.put("prorate", "true");
			response.updateSubscription(subscriptionParams);
			
			return usr;
		}
		catch(Exception e) {
			return null;
		}
	}
	
	
	
	public static ArrayList<DBObject> getAppsAndTables(DBObject user) {
		// Modify the user object to include apps and table names for each app
		ArrayList<DBObject> modifiedApps = new ArrayList<DBObject>();
		
		DBObject apps = (DBObject) user.removeField(Globals.APPS_COLL_NAME);
		ArrayList<String> appdbs = new ArrayList<String>();
		String clientid = (String) user.get(Globals.CLIENT_ID);
		
		if(apps==null)
			return modifiedApps;

		for(String i: apps.keySet()) {
			appdbs.add(clientid + apps.get(i));
		}

		int dbCount = appdbs.size();
		for (int i = 0; i < dbCount; i++) {	
			DBObject d = new BasicDBObject();
			String appName = appdbs.get(i);
			d.put("appname", appName.substring(13));
			d.put(Globals.APIKEY_FIELD_NAME, getApiKey(clientid, appName.substring(13)));
			try 
			{
				MongoClient mc = Utils.getAuthenticatedClient();
				DB appDb = mc.getDB(appName);

				Set<String> tables = appDb.getCollectionNames();

				if(tables.size() != 0) {
					ArrayList<String> tablesArr = new ArrayList<String>(tables);
					tablesArr.remove(tablesArr.size()-1);
					
					d.put("tables", tablesArr);
				} else {
					d.put("tables", null);
				}
				
				modifiedApps.add(d);
				mc.close();

			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}


		}
		return modifiedApps;
	}
	
	/**
	 * Returns api key given client id and app name.
	 * @param clientid
	 * @param appName
	 * @return
	 */
	public static String getApiKey(String clientid, String appName) 
	{
		
		try 
		{
			DBObject query = new BasicDBObject();
			query.put(Globals.CLIENT_ID, clientid);
			query.put("appname", clientid + appName);
			MongoClient mc = Utils.getAuthenticatedClient();
			DB db = mc.getDB(Globals.SYSTEM_DB_NAME);
			
			String retval = db.getCollection(Globals.APPS_COLL_NAME).findOne(query).get(Globals.APIKEY_FIELD_NAME).toString();
			mc.close();
			return retval;

			
		} catch (Exception e) 
		{
			e.printStackTrace();
		}
		
		return null;
	}
	
	/**
	 * Validates the credit card on file for a particular user.
	 * 
	 * @param user
	 * @return
	 */
	public static boolean validateCreditCard(DBObject user)
	{
		// TODO: implement me!
		return true;
	}
	
	
	/**
	 * Stores a stripe connect object into the stripeconnect array that is held within
	 * the user associated with clientid
	 * 
	 * @param clientid
	 * @param stripe
	 * @return - true if stripe object added to the array successfully, false otherwise
	 */
	public static boolean storeStripeConnect(String clientid, DBObject stripe) 
	{
		try 
		{
			// Set up query and update command
			DBObject query = new BasicDBObject();
			DBObject update = new BasicDBObject();
			query.put(Globals.CLIENT_ID, clientid);
		    update.put( "$push", new BasicDBObject("stripeconnect", stripe));
		    
		    // Get the users collection from the database and update
		    MongoClient mc = Utils.getAuthenticatedClient();
			DB db = mc.getDB(Globals.SYSTEM_DB_NAME);
			DBCollection users = db.getCollection(Globals.USERS_COLL_NAME);
			users.update(query, update);
			mc.close();
			return true;
		} 
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return false;
	}
}
