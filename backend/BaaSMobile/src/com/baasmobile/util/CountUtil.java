package com.baasmobile.util;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.WriteConcern;

public class CountUtil
{
	
	/**
	 * Increments the sms count for a particular api key.
	 * This only increments the current day's count.
	 * @param apiKey
	 * @param mc
	 */
	public static void incrementTodaysSmsCount(String apiKey, MongoClient mc)
	{
		DBCollection all_apps = mc.getDB(Globals.SYSTEM_DB_NAME).getCollection(Globals.APPS_COLL_NAME);
		BasicDBObject query = new BasicDBObject(Globals.APIKEY_FIELD_NAME, apiKey);
		String today = todayString();
		
	    BasicDBObject incValue = new BasicDBObject(Globals.SMSCOUNT_FIELD_NAME + "." + today, 1);
	    BasicDBObject intModifier = new BasicDBObject("$inc", incValue);
		all_apps.update(query, intModifier, false, false, WriteConcern.SAFE);

	}
	
	/**
	 * Finds and returns an apps sms count based on the apikey passed in.
	 * @param apiKey
	 * @param mc
	 */
	public static int getTodaysSmsCount(String apiKey, MongoClient mc)
	{
		DBCollection all_apps = mc.getDB(Globals.SYSTEM_DB_NAME).getCollection(Globals.APPS_COLL_NAME);
		BasicDBObject query = new BasicDBObject(Globals.APIKEY_FIELD_NAME, apiKey);
		
		DBObject smsCount = (DBObject) all_apps.findOne(query).get(Globals.APICOUNT_FIELD_NAME);
		String today = todayString();
		
		if(smsCount.containsField(today))
		{
			return (Integer)smsCount.get(today);
		}
		else
		{
			return 0;
		}
	}
	
	/**
	 * Finds and returns an apps sms count based on the apikey passed in.
	 * Returns API count from the specified day.
	 * 
	 * @param apiKey
	 * @param mc
	 */
	public static int getSmsCountFromDate(String apiKey, String date, MongoClient mc)
	{
		DBCollection all_apps = mc.getDB(Globals.SYSTEM_DB_NAME).getCollection(Globals.APPS_COLL_NAME);
		BasicDBObject query = new BasicDBObject(Globals.APIKEY_FIELD_NAME, apiKey);
		
		DBObject smsCount = (DBObject) all_apps.findOne(query).get(Globals.SMSCOUNT_FIELD_NAME);
		
		if(smsCount.containsField(date))
		{
			return (Integer)smsCount.get(date);
		}
		else
		{
			return 0;
		}
	}
	
	/**
	 * Finds and returns an apps sms count based on the apikey passed in.
	 * This method returns the entire DBObject containing all API counts
	 * from all dates.
	 * 
	 * @param apiKey
	 * @param mc
	 */
	public static DBObject getAllSmsCount(String apiKey, MongoClient mc)
	{
		DBCollection all_apps = mc.getDB(Globals.SYSTEM_DB_NAME).getCollection(Globals.APPS_COLL_NAME);
		BasicDBObject query = new BasicDBObject(Globals.APIKEY_FIELD_NAME, apiKey);
		
		DBObject app = all_apps.findOne(query);
		if(!app.containsField(Globals.SMSCOUNT_FIELD_NAME))
			return new BasicDBObject(Globals.SMSCOUNT_FIELD_NAME, new BasicDBObject() );  
		
		DBObject smsCount = (DBObject) app.get(Globals.SMSCOUNT_FIELD_NAME);
		return smsCount;
	}
	
	/**
	 * Increments the api count for a particular api key.
	 * This only increments the current day's count.
	 * @param apiKey
	 * @param mc
	 */
	public static void incrementTodaysApiCount(String apiKey, MongoClient mc)
	{
		DBCollection all_apps = mc.getDB(Globals.SYSTEM_DB_NAME).getCollection(Globals.APPS_COLL_NAME);
		BasicDBObject query = new BasicDBObject(Globals.APIKEY_FIELD_NAME, apiKey);
		String today = todayString();
		
	    BasicDBObject incValue = new BasicDBObject(Globals.APICOUNT_FIELD_NAME + "." + today, 1);
	    BasicDBObject intModifier = new BasicDBObject("$inc", incValue);
		all_apps.update(query, intModifier, false, false, WriteConcern.SAFE);

	}
	
	/**
	 * Finds and returns an apps api count based on the apikey passed in.
	 * @param apiKey
	 * @param mc
	 */
	public static int getTodaysApiCount(String apiKey, MongoClient mc)
	{
		DBCollection all_apps = mc.getDB(Globals.SYSTEM_DB_NAME).getCollection(Globals.APPS_COLL_NAME);
		BasicDBObject query = new BasicDBObject(Globals.APIKEY_FIELD_NAME, apiKey);
		
		DBObject apiCount = (DBObject) all_apps.find(query).next().get(Globals.APICOUNT_FIELD_NAME);
		String today = todayString();
		
		if(apiCount.containsField(today))
		{
			return (Integer)apiCount.get(today);
		}
		else
		{
			return 0;
		}
	}
	
	/**
	 * Finds and returns an apps api count based on the apikey passed in.
	 * Returns API count from the specified day.
	 * 
	 * @param apiKey
	 * @param mc
	 */
	public static int getApiCountFromDate(String apiKey, String date, MongoClient mc)
	{
		DBCollection all_apps = mc.getDB(Globals.SYSTEM_DB_NAME).getCollection(Globals.APPS_COLL_NAME);
		BasicDBObject query = new BasicDBObject(Globals.APIKEY_FIELD_NAME, apiKey);
		
		DBObject apiCount = (DBObject) all_apps.find(query).next().get(Globals.APICOUNT_FIELD_NAME);
		
		if(apiCount.containsField(date))
		{
			return (Integer)apiCount.get(date);
		}
		else
		{
			return 0;
		}
	}
	
	/**
	 * Finds and returns an apps api count based on the apikey passed in.
	 * This method returns the entire DBObject containing all API counts
	 * from all dates.
	 * 
	 * @param apiKey
	 * @param mc
	 */
	public static DBObject getAllApiCount(String apiKey, MongoClient mc)
	{
		DBCollection all_apps = mc.getDB(Globals.SYSTEM_DB_NAME).getCollection(Globals.APPS_COLL_NAME);
		BasicDBObject query = new BasicDBObject(Globals.APIKEY_FIELD_NAME, apiKey);
		
		DBObject app = all_apps.findOne(query);
		if(!app.containsField(Globals.APICOUNT_FIELD_NAME))
			return new BasicDBObject(Globals.APICOUNT_FIELD_NAME, new BasicDBObject() );  
		
		DBObject apiCount = (DBObject) app.get(Globals.APICOUNT_FIELD_NAME);
		return apiCount;
	}
	
	/**
	 * String representation of date. Format is:
	 * 
	 * 				%YYYY_%MM_%DD
	 * 
	 * @return
	 */
	private static String todayString()
	{
		Calendar cal = Calendar.getInstance();
		NumberFormat two_places = new DecimalFormat("00");
		String dateString = cal.get(Calendar.YEAR) + "_" + two_places.format(cal.get(Calendar.MONTH)) +
				"_" + two_places.format(cal.get(Calendar.DAY_OF_MONTH));
		return dateString;
	}

}
