package com.baasmobile.util;

import java.util.ArrayList;
import java.util.Arrays;




/**
 * TODO: change API key to headers - 1
 * TODO: remove valid bit from GET returns - 2 
 * TODO: implement stripe integration - 3
 * TODO: custom code support - 4
 * TODO: push notifications - 5
 * TODO: improved geolocation support - 6 
 * TODO: implement facebook/twitter/insta integration - 7
 * 
 * @author DreW
 *
 */

public class Globals
{
	public static final String DOMAIN = "http://baasmobile.tk";
	public static final String API_DOMAIN = "http://api.baasmobile.tk";
	public static String FREE_PLAN = "Sandbox";
	public static final int API_LIMIT = 100;
	public static final boolean production = true;
	public static ArrayList<String> ORIGINS = new ArrayList<String>(Arrays.asList(
			"http://baasmobile.tk", 
			"http://www.baasmobile.tk", 
			"http://localhost"));
	
	public static final String CONFIG_PATH = "/etc/apache2/";
	public static final String TEST_CONFIG_PATH = "/Users/DreW/git/baas-mobile/backend/BaaSMobile/";
	public static final String CORS_HEADERS = "Access-Control-Allow-Headers";
	public static final String CORS_ORIGINS = "Access-Control-Allow-Origin";
	public static final String CORS_METHODS = "Access-Control-Allow-Methods";
	public static final String ALLOWABLE_METHODS = "GET, POST, PUT, DELETE";
	public static final String ALLOWABLE_HEADERS = "content-type, query, update, apikey, appname, firstname, lastname, email, password, clientid";
	
	public static final String VALID_RECORD = "_valid";
	public static final String CLIENT_ID = "clientid";
	public static final String SYSTEM_DB_NAME = "baasmobile_sys";
	public static final String APPS_COLL_NAME = "apps";
	public static final String USERS_COLL_NAME = "users";
	public static final String APIKEY_FIELD_NAME = "apikey";
	public static final String APICOUNT_FIELD_NAME = "apicount";
	public static final String SMSCOUNT_FIELD_NAME = "smscount";
	public static final String KPI_COLL_NAME = "users";

	public static String STRIPE_API_KEY = "sk_test_Wn1dgQX3dXdiOUxKwh7d21Fe";
	public static final String FILES = "_files";
	public static final String STORAGE = "storage";
	public static String FILES_URL = API_DOMAIN + "/files/";
	
	
	public static String MONGO_HOST;
	public static String MONGO_AUTH_USER;
	public static String MONGO_AUTH_PWD;
	public static String MONGO_AUTH_DB;
	public static int MONGO_PORT;
	
}
