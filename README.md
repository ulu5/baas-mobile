cloudhub
========

Colby Griego
Jash Sayani
Andrew Taeoalii
Jehaz Zarook

- backend: Java project
- frontend: Cloud Hub Website
- demoapps: Demo applications

The zip file contains all components for the project. The backend folder has the java code for the REST API. The frontend folder has the website and dashboard. The demoapps folder contains all 3 demo apps: PictureApp, Appointment website and eMarketplace.


To get the cloudhub backend to work, we got an Amazon EC2 Instance running Amazon Linux. We installed a MongoDB database to interact with our code on that instance. This code will receive commands via a REST API to interact with that specific database through a Java MongoDB driver. Although we have not included MongoDB, Amazon Linux, Jetty server, or Apache server this build can still interface with our real system remotely. Please follow the instructions below to build the interface to our backend.

Backend Build Instructions:

1) Install a Jetty server in Linux environment ( http://docs.codehaus.org/display/JETTY/Downloading+Jetty ) 
2) Open The CloudHub project ( in the backend folder ) in Eclipse IDE for Java EE developers.
3) If it is not a Web Dynamic Project, right click on the project and go to properties. Click on Project Facets and check Dynamic Web Module, Java, and Javascript.
4) Right click on the project and choose "Export"
5) Export as a war file
6) Place the war file in jetty's webapps directory
7) Start jetty using the jetty shell script
8) The server should be running on http://localhost:8080/
9) This installs the backend interaction to the database which will interface with our real database at CloudHub. You can send requests in format: http://localhost:8080/CloudHub/client_id/app_name/table_name?apikey=MY_KEY

Example request: http://localhost:8080/CloudHub/rest/1000106061105/app1/highscores?apikey=1

Frontend Build Instructions:
1) Install and start any web server
2) Place the files from the frontend folder in the "www" directory
3) Opening http://localhost/ should take you to CloudHub homepage

Demo app Instructions:

All demo apps are basic web applications and can be run using any web server.
